// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Provide two-dimensional convolutional kernels for computing the electric potential, electric field
//! and gradients.

use libreda_pnr::db;
use ndarray::Array2;
use num_traits::{Float, FromPrimitive, Zero};
use rustfft::num_complex::Complex;

use super::fft2d;

/// Create a kernel for computing the gradient (by finite differences) of a real valued two-dimensional array
/// of dimension (nx, ny).
/// Gradient in x-axis is represented by the real part, y-axis by the imaginary part.
/// The kernel is nothing more than the weights for the finite differences.
/// The kernel is intended to be transformed into frequency domain and then applied to the frequency
/// domain signal by point-wise multiplication.
///
/// # Panics
/// Panics when `nx`, `ny`, `size_x` or `size_y` is zero.
#[allow(unused)]
pub fn gradient_kernel<F: Float + FromPrimitive>(
    (nx, ny): (usize, usize),
    (size_x, size_y): (F, F),
) -> Array2<Complex<F>> {
    assert_ne!(nx, 0, "nx must be larger than 0.");
    assert_ne!(ny, 0, "nx must be larger than 0.");
    assert!(size_x.is_normal(), "size_x must be a non-zero number.");
    assert!(size_y.is_normal(), "size_y must be a non-zero number.");

    let mut kernel = Array2::zeros((nx, ny));
    let zero = F::zero();
    let two = F::one() + F::one();

    // Compute 1./(finite difference distance)
    let dx = F::from_usize(nx).unwrap() / size_x / two;
    let dy = F::from_usize(ny).unwrap() / size_y / two;
    /*
    kernel_d_core = np.array([
        [0, -1/2j, 0],
        [-1/2, 0, 1/2],
        [0, 1/2j, 0]
    ])
     */
    // kernel[[0, 1]] = Complex::new(dx, _0);
    // kernel[[0, ny - 1]] = Complex::new(-dx, _0);
    //
    // kernel[[1, 0]] = Complex::new(_0, dy);
    // kernel[[nx - 1, 0]] = Complex::new(_0, -dy);

    kernel[[0, 1]] = Complex::new(zero, -dy);
    kernel[[0, ny - 1]] = Complex::new(zero, dy);

    kernel[[1, 0]] = Complex::new(-dx, zero);
    kernel[[nx - 1, 0]] = Complex::new(dx, zero);

    kernel
}

#[test]
fn test_fft_derivative() {
    let (w, h) = (4, 4);
    let size = (4., 2.);

    // Create an all-zero array and set a single value to 1.
    let mut signal = Array2::zeros((w, h));
    signal[[1, 1]] = Complex::new(2., 0.);

    // Compute the derivative.
    let kernel_d_fft = fft2d::fft2(&gradient_kernel((w, h), size));

    let signal_fft = fft2d::fft2(&signal);
    let derivative = fft2d::ifft2(&(signal_fft * kernel_d_fft));

    println!("{:}", derivative);
    let tol = 1e-6;

    assert!((derivative[[1, 1]] - Complex::new(0., 0.)).norm_sqr() < tol);
    assert!((derivative[[0, 0]] - Complex::new(0., 0.)).norm_sqr() < tol);
    assert!((derivative[[2, 2]] - Complex::new(0., 0.)).norm_sqr() < tol);
    assert!((derivative[[0, 2]] - Complex::new(0., 0.)).norm_sqr() < tol);
    assert!((derivative[[2, 0]] - Complex::new(0., 0.)).norm_sqr() < tol);

    assert!((derivative[[1 + 1, 1]] - Complex::new(-1., 0.)).norm_sqr() < tol);
    assert!((derivative[[1 - 1, 1]] - Complex::new(1., 0.)).norm_sqr() < tol);

    assert!((derivative[[1, 1 + 1]] - Complex::new(0., -2.)).norm_sqr() < tol);
    assert!((derivative[[1, 1 - 1]] - Complex::new(0., 2.)).norm_sqr() < tol);
}

/// Create a kernel for computing the second-order gradient (by finite differences) of a real valued two-dimensional array
/// of dimension (nx, ny).
/// The second-order gradient in x-axis is represented by the real part, y-axis by the imaginary part.
/// The kernel is nothing more than the weights for the finite differences.
/// The kernel is intended to be transformed into frequency domain and then applied to the frequency
/// domain signal by point-wise multiplication.
///
/// # Panics
/// Panics when `nx`, `ny`, `size_x` or `size_y` is zero.
pub fn second_order_gradient_kernel<F: Float + FromPrimitive>(
    (nx, ny): (usize, usize),
    (size_x, size_y): (F, F),
) -> Array2<Complex<F>> {
    assert_ne!(nx, 0, "nx must be larger than 0.");
    assert_ne!(ny, 0, "nx must be larger than 0.");
    assert!(size_x.is_normal(), "size_x must be a non-zero number.");
    assert!(size_y.is_normal(), "size_y must be a non-zero number.");

    let mut kernel = Array2::zeros((nx, ny));
    let zero = F::zero();

    // Compute 1./(finite difference distance)
    let dx = F::from_usize(nx).unwrap() / size_x;
    let dy = F::from_usize(ny).unwrap() / size_y;
    /*
    kernel_d_core = np.array([
        [0, -1j, 0],
        [1, -2-2j, 1],
        [0, 1j, 0]
    ])
     */
    kernel[[0, 0]] = Complex::new(-dx - dx, -dy - dy); // (-2*dx -2*dy*i)

    kernel[[0, 1]] = Complex::new(zero, dy);
    kernel[[0, ny - 1]] = Complex::new(zero, dy);

    kernel[[1, 0]] = Complex::new(dx, zero);
    kernel[[nx - 1, 0]] = Complex::new(dx, zero);

    kernel
}

#[test]
fn test_fft_second_order_gradient() {
    let (w, h) = (4, 4);
    let size = (4., 2.);

    // Create an all-zero array and set a single value to 1.
    let mut signal = Array2::zeros((w, h));
    signal[[1, 1]] = Complex::new(1., 0.);

    // Compute the derivative.
    let kernel_dd_fft = fft2d::fft2(&second_order_gradient_kernel((w, h), size));

    let signal_fft = fft2d::fft2(&signal);
    let snd_gradient = fft2d::ifft2(&(signal_fft * kernel_dd_fft));

    println!("{:}", snd_gradient);
    let tol = 1e-6;
    assert!((snd_gradient[[1, 1]] - Complex::new(-2., -4.)).norm_sqr() < tol);
    assert!((snd_gradient[[0, 0]] - Complex::new(0., 0.)).norm_sqr() < tol);

    assert!((snd_gradient[[2, 1]] - Complex::new(1., 0.)).norm_sqr() < tol);
    assert!((snd_gradient[[1, 2]] - Complex::new(0., 2.)).norm_sqr() < tol);
}

/// Generate a convolutional kernel for computing the electric field.
/// Complex numbers are used to represent the E-field vectors.
/// The real part is used for the `x` component, the imaginary part for the `y` component.
///
/// * `(nx, ny)`: Array size.
/// * `(size_x, size_y)`: Spacial size of the kernel.
/// * `cutoff_distance`: Force the kernel values to `0` if they have a manhattan distance larger
/// than `cutoff_distance` to the origin.
///
/// *Note*: When using the kernel to compute an electric field from a charge distribution the
/// *imaginary parts of the charge distribution must be zero*.
/// In other words, the charge density must be encoded in the real part.
pub fn electrostatic_force_kernel<F: Float + FromPrimitive>(
    (nx, ny): (usize, usize),
    (size_x, size_y): (F, F),
    cutoff_distance: (F, F),
) -> Array2<Complex<F>> {
    let f = |xf: F, yf: F| -> Complex<F> {
        // Handle singularity, avoid division by zero.
        let is_zero = xf.is_zero() && yf.is_zero();
        // Force to zero after cutoff distance.
        let is_cutoff = xf.abs() > cutoff_distance.0 || yf.abs() > cutoff_distance.1;

        let e_field = if is_zero || is_cutoff {
            db::Vector::zero()
        } else {
            let position = db::Vector::new(xf, yf);
            let distance = xf.hypot(yf);
            let direction = position / distance;
            direction * (F::one() / position.norm2_squared())
        };
        Complex::new(e_field.x, e_field.y)
    };

    create_kernel((nx, ny), (size_x, size_y), f)
}

/// Generate a convolutional kernel for computing the electric potential.
/// The potential is stored in the real part of the complex values.
///
/// * `(nx, ny)`: Array size.
/// * `(size_x, size_y)`: Spacial size of the kernel.
/// * `cutoff_distance`: Force the kernel values to `0` if they have `x` distance larger
/// than `cutoff_distance.0` to the origin or a `y` distance larger than `cutoff_distance.1` to the origin.
/// by `(size_x, size_y)`.
///
/// *Note*: When using the kernel to compute an electric field from a charge distribution the
/// *imaginary parts of the charge distribution must be zero*.
/// In other words, the charge density must be encoded in the real part.
pub fn electrostatic_potential_kernel<F: Float + FromPrimitive>(
    (nx, ny): (usize, usize),
    (size_x, size_y): (F, F),
    cutoff_distance: (F, F),
) -> Array2<Complex<F>> {
    let f = |xf: F, yf: F| -> Complex<F> {
        // Handle singularity, avoid division by zero.
        let is_zero = xf.is_zero() && yf.is_zero();
        // Force to zero after cutoff distance.
        let is_cutoff = xf.abs() > cutoff_distance.0 || yf.abs() > cutoff_distance.1;

        let potential = if is_zero || is_cutoff {
            F::zero()
        } else {
            let distance = xf.hypot(yf);
            F::one() / distance
        };
        Complex::new(potential, F::zero())
    };

    create_kernel((nx, ny), (size_x, size_y), f)
}

/// Create a two-dimensional discrete convolution kernel from a function `f`.
/// The function will be sampled on a `nx*ny` grid with coordinates covering the rectangle
/// `(-size_x/2, -size_y/2), (size_x/2, size_y/2)1`.
/// The value of `f(0, 0)` will be stored at the index `(0, 0)` of the array. This allows direct use
/// in a FFT based convolution.
///
/// # Type parameters
/// * `F`: Float type.
/// * `Func`: Kernel function to be sampled.
pub fn create_kernel<F, Func>(
    (nx, ny): (usize, usize),
    (size_x, size_y): (F, F),
    f: Func,
) -> Array2<Complex<F>>
where
    F: Float + FromPrimitive,
    Func: Fn(F, F) -> Complex<F>,
{
    let mut kernel = Array2::zeros((nx, ny));

    let center_x = nx as isize / 2;
    let center_y = ny as isize / 2;

    // Size of a single pixel.
    let unit_x = size_x / F::from_usize(nx).unwrap();
    let unit_y = size_y / F::from_usize(ny).unwrap();

    for i in 0..nx {
        for j in 0..ny {
            let x = i as isize - center_x;
            let y = j as isize - center_y;
            let xf = F::from_isize(x).unwrap() * unit_x;
            let yf = F::from_isize(y).unwrap() * unit_y;
            kernel[[i, j]] = f(xf, yf);
        }
    }

    // Shift the center of the kernel to [0, 0].
    fft2d::ifftshift_inplace(&mut kernel);
    kernel
}

#[test]
fn test_efield_kernel_zero_at_origin() {
    let (w, h) = (4, 4);
    let kernel = electrostatic_force_kernel::<f64>((w, h), (4.0, 4.0), (2.0, 2.0));
    println!("{}", kernel);
    // E-field at the origin must be (0, 0).
    assert!(kernel[[0, 0]].is_zero());
}

#[test]
fn test_efield_computation() {
    // Compute e-field by convolving a charge distribution with the kernel.

    let (w, h) = (8, 8);

    // Create charge distribution.
    let mut charges = Array2::zeros((w, h));
    // Set a charge.
    let charge_x = w / 2;
    let charge_y = h / 2;
    charges[[charge_x, charge_y]] = Complex::new(1.0f64, 0.);

    // Convolution kernel in spacial domain.
    let kernel = electrostatic_force_kernel::<f64>((w, h), (8., 8.), (4., 4.));

    // To spectral domain.
    let kernel_fft = fft2d::fft2(&kernel);
    let charges_fft = fft2d::fft2(&charges);

    // Point-wise multiply in spectral domain <-> convolution in spacial domain.
    // Then back to spacial domain.
    let e_field = fft2d::ifft2(&(kernel_fft * charges_fft));

    // println!("{}", e_field);

    assert!(
        e_field[[charge_x, charge_y]].norm() < 1e-6,
        "E-field at the location of point charge is defined to be zero."
    );

    // Check signs of field components.
    assert!(
        e_field[[charge_x - 1, charge_y]].re < 0.0,
        "Expect field in negative x direction."
    );
    assert!(
        e_field[[charge_x + 1, charge_y]].re > 0.0,
        "Expect field in positive x direction."
    );
    assert!(
        e_field[[charge_x - 1, charge_y]].im.abs() < 1e-6,
        "Expect no field in y direction."
    );
    assert!(
        e_field[[charge_x + 1, charge_y]].im.abs() < 1e-6,
        "Expect no field in y direction."
    );

    assert!(
        e_field[[charge_x, charge_y - 1]].im < 0.0,
        "Expect field in negative y direction."
    );
    assert!(
        e_field[[charge_x, charge_y + 1]].im > 0.0,
        "Expect field in positive y direction."
    );
    assert!(
        e_field[[charge_x, charge_y - 1]].re.abs() < 1e-6,
        "Expect no field in x direction."
    );
    assert!(
        e_field[[charge_x, charge_y + 1]].re.abs() < 1e-6,
        "Expect no field in x direction."
    );
}
