// SPDX-FileCopyrightText: 2023 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Data-structures for constructing charge distributions and computing the resulting electric fields.

use libreda_pnr::db;
use libreda_pnr::db::traits::*;
use libreda_pnr::prelude::placement_density::DensityMap;
use ndarray::Array2;
use num_traits::{Float, FromPrimitive, NumAssign, Zero};
use rayon::prelude::{IntoParallelIterator, ParallelIterator};
use rustfft::num_complex::Complex;

use crate::eplace_ms::fft2d;

use super::{convolution_kernels::*, fft2d::FFTFloat};

/// Struct for creating electric charge distributions and computing their electric fields.
pub struct EFieldKernel<F> {
    /// Geometrical size of the canvas.
    shape: db::Rect<F>,
    /// Resolution of the matrix.
    size: (usize, usize),
    /// Convolution kernel for computing gradient of electric potential (electric field) in frequency domain.
    kernel_potential_gradient: Array2<Complex<F>>,
    /// Convolutional kernel for computing second order gradient of electric potential.
    kernel_potential_second_order_gradient: Array2<Complex<F>>,
}

/// Struct for creating electric charge distributions and computing their electric fields.
/// Use `EFieldKernel::new_density_map()` to create an instance of this struct.
#[derive(Clone)]
pub struct EDensity<F> {
    /// Electric charge distribution.
    pub charge_distribution: DensityMap<F, Complex<F>>,
}

/// Struct that holds the result of an electric field computation.
pub struct EField<'a, F> {
    pub charge_distribution: &'a DensityMap<F, Complex<F>>,
    /// Buffer for holding electric field.
    potential_gradient: Array2<Complex<F>>,
    /// Buffer for holding the second order gradient of the electric potential..
    potential_second_order_gradient: Array2<Complex<F>>,
}

impl<'a, F> EField<'a, F>
where
    F: Float + Copy + Default + FromPrimitive + NumAssign + std::fmt::Debug,
{
    /// Compute force `F` on a point charge at location `p` with a given `charge`.
    /// Also compute derivatives `F_x/dx` and `F_y/dy`.
    #[allow(unused)]
    pub fn compute_force_on_point_charge(
        &self,
        p: db::Point<F>,
        charge: F,
    ) -> (db::Vector<F>, db::Vector<F>) {
        // Represent the point charge as a square shape of unit size.
        let half = F::from_f64(0.5).unwrap();
        let r = db::Rect::new(db::Point::new(-half, -half), db::Point::new(half, half))
            .translate(p.into());

        self.compute_force(&r, charge)
    }

    /// Compute force `F` on a rectangle `r` with a given `charge_density`.
    /// Also compute derivatives `F_x/dx` and `F_y/dy`.
    pub fn compute_force(
        &self,
        r: &db::Rect<F>,
        charge_density: F,
    ) -> (db::Vector<F>, db::Vector<F>) {
        // Compute force and force derivatives (f_x/dx and f_y/dy).

        assert!(
            self.charge_distribution.dimension.contains_rectangle(r),
            "Rectangle is not inside boundary of charge distribution."
        );

        // Find indices of pixels that interact with the corners of the rectangle `r`.
        let (xstart, ystart) = self
            .charge_distribution
            .coordinates_to_indices(r.lower_left);
        let (xend, yend) = self
            .charge_distribution
            .coordinates_to_indices(r.upper_right);

        let mut force: db::Vector<F> = Zero::zero();
        let mut force_gradient: db::Vector<F> = Zero::zero();

        let pixel_area = self.charge_distribution.bin_area();

        // Loop over all pixels that interact with the rectangle `r`.
        for x in xstart..xend + 1 {
            for y in ystart..yend + 1 {
                // Compute how much r overlaps with the current pixel.
                let overlap_area = if x == xstart || x == xend || y == ystart || y == yend {
                    // Corner and edge cases with partial overlap.
                    // Compute increment by the overlap of the pixel shape and the rectangle.
                    let pixel_shape = self.charge_distribution.get_bin_shape((x, y));

                    // Get the intersection of the pixel shape and the component shape.
                    pixel_shape
                        .intersection(r)
                        .map(|r| r.width() * r.height())
                        // .expect("Pixel must intersect with rectangle!");
                        .unwrap_or(F::zero())
                } else {
                    // Pixel is fully covered by the rectangle r.
                    pixel_area
                };

                let f = self.potential_gradient[[x, y]] * overlap_area;
                let f_gradient = self.potential_second_order_gradient[[x, y]] * overlap_area;

                force += db::Vector::new(f.re, f.im);
                force_gradient += db::Vector::new(f_gradient.re, f_gradient.im);
            }
        }
        (force * charge_density, force_gradient * charge_density)
    }

    // /// Compute the force and force gradient on a point charge at location `p`.
    // fn compute_force_on_point(&self, p: &db::Point<F>, charge: F) -> (db::Vector<F>, db::Vector<F>) {
    //     // Compute force and force derivatives (f_x/dx and f_y/dy).
    //
    //     assert!(self.charge_distribution.dimension.contains_point(p), "Point is not inside boundary of charge distribution.");
    //
    //     let (x, y) = p.into();
    //
    //     let f = self.potential_gradient[[x, y]] * charge;
    //     let f_gradient = self.potential_second_order_gradient[[x, y]] * charge;
    //
    //     let force = db::Vector::new(f.re, f.im);
    //     let force_gradient = db::Vector::new(f_gradient.re, f_gradient.im);
    //
    //     (force, force_gradient)
    // }
}

impl<F> EFieldKernel<F>
where
    F: Float + Copy + Default + FromPrimitive + FFTFloat + NumAssign + std::fmt::Display,
{
    /// Allocate space and pre-compute convolutional kernels in spacial domain.
    pub fn new(r: db::Rect<F>, (w, h): (usize, usize)) -> Self {
        // Prepare convolutional kernel for computing the potential.
        let potential_kernel_fft = {
            let two = F::from_usize(2).unwrap();
            let size = (r.width(), r.height());
            let cutoff_distance = (size.0 / two, size.1 / two);
            let mut potential_kernel =
                electrostatic_potential_kernel((w, h), size, cutoff_distance);

            //{
            //    let potential_kernel_real = potential_kernel.map(|c| c.re);
            //    dump_array2("/tmp/potential_kernel.csv", &potential_kernel_real);
            //}

            // Convert to spectral representation.
            fft2d::fft2_forward_inplace(&mut potential_kernel);
            potential_kernel
        };

        //// Prepare convolutional kernel for computing the gradient of the potential (e-field).
        //let potential_gradient_kernel_fft = {
        //    let dim = density_bins.get_data_ref().dim();
        //    let size = (
        //        density_bins.dimension.width(),
        //        density_bins.dimension.height(),
        //    );
        //    let mut gradient_kernel = super::convolution_kernels::gradient_kernel(dim, size);

        //    // Convert to spectral representation.
        //    fft2d::fft2_forward_inplace(&mut gradient_kernel);

        //    gradient_kernel * &potential_kernel_fft
        //};

        // Prepare convolutional kernel for computing the gradient of the potential (e-field).
        let potential_gradient_kernel_fft = {
            let two = F::from_usize(2).unwrap();
            let size = (r.width(), r.height());
            let cutoff_distance = (size.0 / two, size.1 / two);
            let mut electric_force_kernel =
                electrostatic_force_kernel((w, h), size, cutoff_distance);

            // Convert to spectral representation.
            fft2d::fft2_forward_inplace(&mut electric_force_kernel);
            electric_force_kernel
        };

        // Prepare convolutional kernel for computing the second order gradient of the potential.
        // Used for computing the hessian pre-conditioning factors.
        let potential_second_order_gradient_kernel_fft = {
            let size = (r.width(), r.height());
            let mut snd_order_gradient_kernel = second_order_gradient_kernel((w, h), size);
            // Convert to spectral representation.
            fft2d::fft2_forward_inplace(&mut snd_order_gradient_kernel);

            snd_order_gradient_kernel * &potential_kernel_fft
        };
        Self {
            shape: r,
            size: (w, h),
            kernel_potential_gradient: potential_gradient_kernel_fft,
            kernel_potential_second_order_gradient: potential_second_order_gradient_kernel_fft,
        }
    }

    /// Create a canvas for drawing charge densities.
    /// The canvas borrows the convolutional kernels from `self` for computation of the E-field.
    /// The canvas has the same dimensions as the kernel.
    pub fn new_density_map(&self) -> EDensity<F> {
        EDensity {
            charge_distribution: DensityMap::new(self.shape, self.size),
        }
    }
}

impl<F> EDensity<F>
where
    F: Float + Copy + Default + FromPrimitive + FFTFloat + NumAssign + std::fmt::Display,
{
    /// Set the charge distribution to zero.
    pub fn clear(&mut self) {
        self.set(F::zero())
    }

    /// Set all values.
    pub fn set(&mut self, value: F) {
        self.charge_distribution
            .data
            .iter_mut()
            .for_each(|v| *v = Complex::new(value, F::zero()));
    }

    /// Add a charge density of rectangular shape.
    pub fn draw_charge_density(&mut self, r: &db::Rect<F>, charge_density: F) {
        debug_assert!(self.charge_distribution.dimension.contains_rectangle(r));
        self.charge_distribution
            .draw_rect(r, Complex::new(charge_density, F::zero()));
    }

    /// Add a point-charge.
    #[allow(unused)]
    pub fn draw_point_charge(&mut self, p: db::Point<F>, charge: F) {
        let half = F::from_f64(0.5).unwrap();
        let r = db::Rect::new(db::Point::new(-half, -half), db::Point::new(half, half))
            .translate(p.into());

        self.draw_charge_density(&r, charge);
    }

    /// Compute the electric field and second order gradient of the potential.
    pub fn compute_efield(&self, kernel: &EFieldKernel<F>) -> EField<F> {
        assert_eq!(kernel.size, self.charge_distribution.get_data_ref().dim());
        assert_eq!(kernel.shape, self.charge_distribution.dimension());

        // FFT convolve.
        // TODO: Do with less allocations? Maybe does not matter.
        let fft = fft2d::fft2(&self.charge_distribution.data);
        // let potential = fft2d::ifft2(&(&fft * &self.potential_kernel_fft));

        // Compute the gradient and second order gradient in parallel.
        let kernels = vec![
            &kernel.kernel_potential_gradient,
            &kernel.kernel_potential_second_order_gradient,
        ];

        let mut convolutions: Vec<_> = kernels
            .into_par_iter() // Parallel iterator.
            .map(|kernel| fft2d::ifft2(&(&fft * kernel)))
            .collect();

        let potential_second_order_gradient = convolutions.pop().unwrap();
        let potential_gradient = convolutions.pop().unwrap();

        EField {
            charge_distribution: &self.charge_distribution,
            potential_gradient,
            potential_second_order_gradient,
        }
    }
}

#[test]
fn test_compute_efield_single_particle() {
    let kernel = EFieldKernel::new(db::Rect::new((0., 0.), (10., 10.)), (10, 10));
    let mut density = kernel.new_density_map();

    let particle = db::Rect::new((5.0, 5.0), (6.0, 6.0));
    density.draw_charge_density(&particle, 1.);

    let efield = density.compute_efield(&kernel);

    let (force, _force_derivative) = efield.compute_force(&particle, 2.);

    let tol = 1e-6;
    // dbg!(force_derivative);
    assert!(
        force.x.abs() < tol,
        "electric force on particle should be zero"
    );
    assert!(
        force.y.abs() < tol,
        "electric force on particle should be zero"
    );
    // assert!(force_derivative.x.abs() < tol);
    // assert!(force_derivative.y.abs() < tol);

    let displacements = [
        (1., 0.),
        (-1., 0.),
        (0., 1.),
        (0., -1.),
        (3., 0.),
        (0., 3.),
        (2., 2.),
        (-2., 2.),
        (1.7, 0.),
        (0., 2.3),
        (1.7, 2.3),
        // With overlap:
        (0.5, 0.),
        (0.1, 0.1),
    ];

    for d in displacements {
        let displacement = db::Vector::from(d);
        dbg!(displacement);

        let particle2 = particle.translate(displacement);
        let (force, force_derivative) = efield.compute_force(&particle2, 1.);

        dbg!(force);
        dbg!(force_derivative);

        assert!(force.norm2() > 0.01, "force must be non-zero");

        // Check if parallel.
        assert!(
            displacement.dot(force.rotate_ortho(db::Angle::R90)).abs()
                < 0.1 * force.norm2() * displacement.norm2(),
            "force must be parallel to the displacement"
        );
    }
    // {
    //     let d = 1e-8;
    //     let neg = db::Rect::new((6.-d, 5.), (6.-d, 6.));
    //     let pos = db::Rect::new((6.+d, 5.), (6.+d, 6.));
    //     let (force_neg, _) = efield.compute_force(&neg, 1.);
    //     let (force_pos, _) = efield.compute_force(&pos, 1.);
    //     let derivative = (force_pos - force_neg) / (d*2.);
    //     dbg!(derivative);
    // }
    // {
    //     let d = 1e-8;
    //     let neg = db::Rect::new((6., 5.-d), (7., 6.-d));
    //     let pos = db::Rect::new((6., 5.+d), (7., 6.+d));
    //     let (force_neg, _) = efield.compute_force(&neg, 1.);
    //     let (force_pos, _) = efield.compute_force(&pos, 1.);
    //     let derivative = (force_pos - force_neg) / (d*2.);
    //     dbg!(derivative);
    // }
}

#[test]
fn test_compute_efield_two_particles() {
    let kernel = EFieldKernel::new(db::Rect::new((0., 0.), (100., 100.)), (100, 100));
    let mut density = kernel.new_density_map();

    let particle1 = db::Rect::new((40.0, 40.3), (50.4, 50.0));
    let displacement = db::Vector::new(1.23, 4.56);
    let particle2 = particle1.translate(displacement);
    density.draw_charge_density(&particle1, 1.);
    density.draw_charge_density(&particle2, 1.);

    let efield = density.compute_efield(&kernel);

    let (force1, _force_derivative) = efield.compute_force(&particle1, 1.);
    let (force2, _force_derivative) = efield.compute_force(&particle2, 1.);
    dbg!(force1, force2);

    assert!(force1.norm2() > 0.01, "force must be non-zero");
    assert!(
        (force1 + force2).norm2() < 1e-6,
        "sum of forces must be zero"
    );

    // Particles should feel a repulsing force.
    assert_eq!(force2.x.signum(), displacement.x.signum());
    assert_eq!(force2.y.signum(), displacement.y.signum());
    assert_eq!(force1.x.signum(), -displacement.x.signum());
    assert_eq!(force1.y.signum(), -displacement.y.signum());
}

#[test]
fn test_compute_efield_large_object() {
    let kernel = EFieldKernel::new(db::Rect::new((0., 0.), (100., 100.)), (100, 100));
    let mut density = kernel.new_density_map();

    let particle = db::Rect::new((10.0, 10.0), (50.0, 50.0));
    density.draw_charge_density(&particle, 1.);

    let efield = density.compute_efield(&kernel);

    let (force, _force_derivative) = efield.compute_force(&particle, 1.);

    let tol = 1e-6;
    // dbg!(force_derivative);
    assert!(
        force.x.abs() < tol,
        "electric force on particle should be zero"
    );
    assert!(
        force.y.abs() < tol,
        "electric force on particle should be zero"
    );
    // assert!(force_derivative.x.abs() < tol);
    // assert!(force_derivative.y.abs() < tol);

    let displacements = [
        (1., 0.),
        (-1., 0.),
        (0., 1.),
        (0., -1.),
        (10., 10.),
        (3., 0.),
        (0., 3.),
        (2., 2.),
        (-2., 2.),
        (1.7, 0.),
        (0., 2.3),
        (1.7, 2.3),
        (0.5, 0.),
        (0.1, 0.1),
    ];

    for d in displacements {
        let displacement = db::Vector::from(d);
        dbg!(displacement);

        let particle2 = particle.translate(displacement);
        let (force, force_derivative) = efield.compute_force(&particle2, 1.);

        dbg!(force);
        dbg!(force_derivative);

        assert!(force.norm2() > 0.01, "force must be non-zero");

        // Check if parallel.
        assert!(
            displacement.dot(force.rotate_ortho(db::Angle::R90)).abs()
                < 0.1 * force.norm2() * displacement.norm2(),
            "force must be parallel to the displacement"
        );
    }
}
