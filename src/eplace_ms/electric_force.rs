// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Compute the electrostatic force on a charged rectangle given an electric potential.

use super::line_integral::line_integral;
use libreda_pnr::db;
use num_traits::{Float, FromPrimitive, Num, ToPrimitive, Zero};
use std::ops::Mul;

/// Numerically integrate the function `potential` along the boundary of `r` wighted with the normal
/// on the boundary.
///
/// Used for:
/// Compute the force on the rectangle `r` with charge density `r_charge_density` resulting
/// from the electrostatic `potential`.
///
/// Instead of integrating the electric field multiplied the charge density over an area
/// here we integrate the potential along the border of the area multiplied with the normal vector.
/// This is in principle a gradient computation via finite differences.
///
/// This is meant for force computation on large macro blocks.
///
/// * `potential`: A function that maps `(x, y)` coordinates onto a potential (scalar value).
/// * `sample_distance`: Approximate spacial distance between samples for the numerical integration.
/// This should be chosen according to the smoothness of the `potential` function.
#[allow(unused)]
pub fn boundary_integral<F, Z, P: Fn((F, F)) -> Z>(
    potential: &P,
    r: db::Rect<F>,
    sample_distance: F,
) -> db::Vector<Z>
where
    F: Float + FromPrimitive + ToPrimitive,
    Z: Num + Zero + Copy + Mul<F, Output = Z>,
{
    debug_assert!(sample_distance.is_sign_positive());

    let mut force_sum = db::Vector::zero();

    let edges: [db::Edge<F>; 4] = [
        db::Edge::new(r.lower_left(), r.lower_right()),
        db::Edge::new(r.lower_right(), r.upper_right()),
        db::Edge::new(r.upper_right(), r.upper_left()),
        db::Edge::new(r.upper_left(), r.lower_left()),
    ];

    // Assemble the boundary integral as sum of line integrals.
    for edge in edges {
        let v = edge.vector();
        let l = v.norm2();
        let n_samples = (l / sample_distance).ceil().to_usize().unwrap();
        let step: db::Vector<_> = v / (F::from_usize(n_samples)).unwrap();

        let sample_points =
            (0..n_samples + 1).map(|i| edge.start + step * F::from_usize(i).unwrap());

        // Integrate the potential along the edge.
        let line_sum = line_integral(sample_points, potential);

        // Force direction is perpendicular to the edge.
        let normal = v.normal();
        force_sum = force_sum + db::Vector::new(line_sum * normal.x, line_sum * normal.y);
        // Accumulate the force.
    }

    force_sum
}

#[test]
fn test_compute_electric_force_constant_potential() {
    let r = db::Rect::new((0., 0.), (10., 20.));

    let force = boundary_integral(&|(_x, _y)| 1., r, 1.0);

    assert!(
        force.norm2() < 1e-12,
        "Force on an object in a constant potential should be zero."
    );
}

#[test]
fn test_compute_electric_force_linear_gradient() {
    let r = db::Rect::new((0., 0.), (10., 20.));

    let force = boundary_integral(
        &|(x, _y)| x, // Potential gets higher towards high x.
        r,
        1.0,
    );

    assert!(force.x < 0., "Expect a force in negative x-direction.");
    assert!(
        force.y.abs() < 1e-12,
        "Expect a force in negative x-direction."
    );
}
