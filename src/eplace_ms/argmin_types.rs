// SPDX-FileCopyrightText: 2023 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Data structures for optimization parameter and gradient.

use std::ops::Deref;
use std::ops::DerefMut;

use argmin_math::ArgminAdd;
use argmin_math::ArgminL2Norm;
use argmin_math::ArgminMul;
use argmin_math::ArgminScaledSub;
use libreda_pnr::db;
use ndarray::Array1;
use num_traits::Float;
use num_traits::Zero;
use rayon::prelude::IndexedParallelIterator;
use rayon::prelude::IntoParallelRefIterator;
use rayon::prelude::ParallelIterator;

use crate::kahan_sum::ParallelKahanSum;

/// Wrapper around `Vec` which simplifies parallel elementwise operations.
#[derive(Clone, Debug)]
pub struct ParVec<T> {
    pub inner: Vec<T>,
}

impl<T> std::ops::Index<usize> for ParVec<T> {
    type Output = T;

    fn index(&self, index: usize) -> &Self::Output {
        &self.inner[index]
    }
}

impl<T> std::ops::IndexMut<usize> for ParVec<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.inner[index]
    }
}

impl<T> AsRef<[T]> for ParVec<T> {
    fn as_ref(&self) -> &[T] {
        &self.inner
    }
}

impl<T> AsMut<[T]> for ParVec<T> {
    fn as_mut(&mut self) -> &mut [T] {
        &mut self.inner
    }
}

//impl<'data, T> IntoParallelRefIterator<'data> for Param<T>
//where
//    T: Sync,
//{
//    type Iter;
//
//    type Item = &'data T;
//
//    fn par_iter(&'data self) -> Self::Iter {
//        self.param.par_iter()
//    }
//}

impl<T> ParVec<T> {
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }

    pub fn zeros(len: usize) -> Self
    where
        T: Zero,
    {
        Self {
            inner: (0..len).map(|_| Zero::zero()).collect(),
        }
    }

    pub fn map<U>(&self, f: impl (Fn(&T) -> U) + Sync) -> ParVec<U>
    where
        T: Sync,
        U: Send,
    {
        ParVec {
            inner: self.inner.par_iter().map(|v| f(v)).collect(),
        }
    }

    pub fn zip_map<U, V>(&self, other: &ParVec<U>, f: impl (Fn(&T, &U) -> V) + Sync) -> ParVec<V>
    where
        T: Sync,
        U: Sync,
        V: Send,
    {
        ParVec {
            inner: self
                .inner
                .par_iter()
                .zip_eq(&other.inner)
                .map(|(a, b)| f(a, b))
                .collect(),
        }
    }
}

impl<T> Deref for ParVec<T> {
    type Target = Vec<T>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> DerefMut for ParVec<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

impl<T> From<Vec<T>> for ParVec<T> {
    fn from(v: Vec<T>) -> Self {
        Self { inner: v }
    }
}

impl<T> From<Array1<T>> for ParVec<T> {
    fn from(param: Array1<T>) -> Self {
        Self {
            inner: param.into_raw_vec(),
        }
    }
}

impl<T> Into<Array1<T>> for ParVec<T> {
    fn into(self) -> Array1<T> {
        Array1::from_vec(self.inner)
    }
}

impl<T> Into<Vec<T>> for ParVec<T> {
    fn into(self) -> Vec<T> {
        self.inner
    }
}

impl<F> ArgminL2Norm<F> for ParVec<db::Vector<F>>
where
    F: Float + Sync + Send,
{
    fn l2_norm(&self) -> F {
        self.inner
            .par_iter()
            .map(|v| v.norm2_squared())
            // TODO: Is Kahan-sum necessary or would a simple `.sum()` be good enough?
            .kahan_sum()
            .sqrt()
    }
}

impl<T> ArgminMul<T, Self> for ParVec<T>
where
    T: Copy + std::ops::Mul<Output = T> + Sync + Send,
{
    fn mul(&self, &other: &T) -> Self {
        self.map(|&v| v * other)
    }
}

// TODO:
//impl<T> ArgminScaledAdd<Self, T, Self> for ParVec<T>
//where
//    T: Copy + std::ops::Mul<Output = T> + std::ops::Add<Output = T>,
//{
//    fn scaled_add(&self, factor: &f64, vec: &Self) -> Self {
//        self.zip_map(vec, |&a, &b| a + b * factor)
//    }
//}

impl<T> ArgminScaledSub<Self, T, Self> for ParVec<T>
where
    T: Copy + std::ops::Mul<T, Output = T> + std::ops::Sub<Output = T> + Sync + Send,
{
    fn scaled_sub(&self, &factor: &T, vec: &Self) -> Self {
        self.zip_map(vec, |&a, &b| a - b * factor)
    }
}

impl<T> ArgminAdd<Self, Self> for ParVec<T>
where
    T: Copy + std::ops::Add<Output = T> + Sync + Send,
{
    fn add(&self, vec: &Self) -> Self {
        self + vec
    }
}

impl<T> std::ops::Add<&ParVec<T>> for &ParVec<T>
where
    T: Copy + std::ops::Add<Output = T> + Sync + Send,
{
    type Output = ParVec<T>;

    fn add(self, rhs: &ParVec<T>) -> Self::Output {
        self.zip_map(rhs, |&a, &b| a + b)
    }
}

impl<T> std::ops::Sub<&ParVec<T>> for &ParVec<T>
where
    T: Copy + std::ops::Sub<Output = T> + Sync + Send,
{
    type Output = ParVec<T>;

    fn sub(self, rhs: &ParVec<T>) -> Self::Output {
        self.zip_map(rhs, |&a, &b| a - b)
    }
}

impl<T> std::ops::Mul<T> for &ParVec<T>
where
    T: Copy + std::ops::Mul<Output = T> + Sync + Send,
{
    type Output = ParVec<T>;

    fn mul(self, rhs: T) -> Self::Output {
        self.map(|&v| v * rhs)
    }
}

impl<T> std::ops::Div<T> for &ParVec<T>
where
    T: Copy + std::ops::Div<Output = T> + Sync + Send,
{
    type Output = ParVec<T>;

    fn div(self, rhs: T) -> Self::Output {
        self.map(|&v| v / rhs)
    }
}

#[test]
fn test_l2_norm() {
    let a = ParVec::from(vec![-3., 0., 4.]);

    let norm: f64 = a.l2_norm();

    assert!((norm - 5.).abs() < 1e-12);
}

#[test]
fn test_parvec_scaled_sub() {
    let a = ParVec::from(vec![1., 2., 3.]);
    let b = ParVec::from(vec![0.1, 0.2, 0.3]);

    let c = a.scaled_sub(&2., &b);

    let expected = ParVec::from(vec![0.8, 1.6, 2.4]);

    assert!((&c - &expected).l2_norm() < 1e-9);
}

#[test]
fn test_parvec_mul() {
    let a = ParVec::from(vec![1., 2., 3.]);

    let b = &a * 2.;

    let expected = ParVec::from(vec![2., 4., 6.]);

    assert!((&b - &expected).l2_norm() < 1e-9);
}
