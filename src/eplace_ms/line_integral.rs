// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Integrate scalar fields along a poly-line.

#![allow(unused)]

use libreda_pnr::db;
use num_traits::{Float, Num};
use std::ops::Mul;

/// Integrate values along a poly-line.
/// Sample the values of the field at each vertex of the poly-line
/// and integrate them using the trapezoid rule.
pub fn line_integral<F, Z, I, Field>(points: I, field: &Field) -> Z
where
    F: Float,
    Z: Num + Copy + Mul<F, Output = Z>,
    I: Iterator<Item = db::Point<F>>,
    Field: Fn((F, F)) -> Z,
{
    let mut integral = Z::zero();

    let mut previous: Option<(db::Point<F>, Z)> = None;

    points.for_each(|p| {
        // Sample field value with 2D interpolation.
        let value: Z = field(p.into());

        if let Some((prev_p, prev_value)) = previous {
            // Distance between the samples.
            let dist = (p - prev_p).norm2();
            integral = integral + (value + prev_value) * dist; // Division by 2 is done in the end on the whole sum.
        }

        previous = Some((p, value));
    });

    integral / (Z::one() + Z::one())
}

#[test]
fn test_line_integral() {
    let sample_points: Vec<db::Point<_>> = vec![(0., 0.), (1., 0.), (0., -1.)]
        .iter()
        .map(|p| p.into())
        .collect();

    let integral = line_integral(
        sample_points.iter().copied(),
        &|(x, y)| x + y, // 2D scalar field.
    );

    assert!((integral - 1.) < 1e-12);
}
