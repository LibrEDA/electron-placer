// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Rust implementation of the ePlace-MS mixed-size placement algorithm.
//!
//! ePlace is a force-driven analytic placement algorithm which
//! treats standard-cells and macro blocks in the same way and hence places them
//! in the same placement step.
//!
//! The forces acting on the cells are a sum of electro-static repulsion between cells
//! and a force that pulls together connected cells. Repulsion helps achieving the density constraint
//! while the contracting forces are used to optimize the wiring length.
//!
//! Electrostatic forces are efficiently computed by an FFT based convolution.
//!
//! This implementation follows the core ideas of ePlace (electro-static density penalty,
//! wire-length model). The implementation however, is different and there is also room for
//! improvement in the optimization steps (hessian pre-conditioning is not done yet, step-length
//! back-tracking is not done yet).
//!
//! [`MixedSizeGlobalPlacer`] does the global placement of macro blocks and standard-cells based
//! on an initial-placement guess.
//!
//! [`AnnealingMacroLegalizer`] is used for legalizing the macros without the standard-cells. It
//! can also be used on its own, detached from ePlace.
//!
//! [`EPlaceMS`] assembles the above engines into a placement engine.
//!
//!
//! # References
//! * ePlace-MS: <https://cseweb.ucsd.edu/~jlu/papers/eplace-ms-tcad14/paper.pdf>
//! * Fast Electrostatic Halftoning: <https://www.mia.uni-saarland.de/Publications/gwosdek-pp295.pdf>
//! (This is a very similar problem just without optimizing any wire length.)

mod argmin_types;
mod convolution_kernels;
mod efield;
mod electric_force;
mod eplace_ms;
mod fft2d;
mod half_perimeter_wire_length;
mod line_integral;
mod macro_legalizer;
mod mixed_size_global_placement;
mod nesterov_gradient_descent;

// Public exports.
pub use eplace_ms::EPlaceMS;
pub use macro_legalizer::AnnealingMacroLegalizer;
pub use mixed_size_global_placement::MixedSizeGlobalPlacer;
