// SPDX-FileCopyrightText: 2023 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Nesterov's accelerated gradient descent.
//!
//! See: https://web.archive.org/web/20210302210908/https://blogs.princeton.edu/imabandit/2013/04/01/acceleratedgradientdescent/

use argmin::{
    argmin_error, argmin_error_closure,
    core::{
        ArgminFloat, DeserializeOwnedAlias, Error, Gradient, IterState, LineSearch, Problem,
        SerializeAlias, Solver, State, TerminationReason, TerminationStatus, KV,
    },
    float,
};
use argmin_math::{ArgminL2Norm, ArgminMul, ArgminScaledAdd, ArgminScaledSub, ArgminSub};
use serde::{Deserialize, Serialize};

/// Solver using Nesterov's accelerated gradient descent.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Nesterov<P, F> {
    y: Option<P>,
    step_length: Option<F>,
    initial_step_length: F,
    target_gradient_l2_norm: F,
}

impl<P, F> Nesterov<P, F>
where
    F: ArgminFloat,
{
    /// Constructor.
    pub fn new() -> Self {
        Self {
            y: None,
            step_length: None,
            initial_step_length: float!(1e-6),
            target_gradient_l2_norm: float!(1e-6),
        }
    }
}

impl<P, F> Nesterov<P, F>
where
    F: ArgminFloat,
{
    fn next_lambda(lambda_prev: F) -> F
    where
        F: ArgminFloat,
    {
        (F::one() + F::sqrt(F::one() + float!(4.) * lambda_prev.powi(2))) / float!(2.)
    }

    fn gamma(lambda_prev: F, lambda: F) -> F
    where
        F: ArgminFloat,
    {
        debug_assert_ne!(lambda, F::zero(), "lambda must not be zero");

        let gamma = (F::one() - lambda_prev) / lambda;

        debug_assert!(
            gamma.is_one() // gamma_0
             || gamma <= F::zero(),
            "gamma_s must be <= 0 for s != 0"
        );

        gamma
    }
}

impl<O, F, P, G> Solver<O, IterState<P, G, (), (), F>> for Nesterov<P, F>
where
    O: Gradient<Param = P, Gradient = G>,
    P: Clone
        + std::fmt::Debug
        + ArgminScaledSub<G, F, P>
        + ArgminScaledAdd<P, F, P>
        + ArgminScaledAdd<G, F, P>
        + ArgminMul<F, P>
        + ArgminSub<P, P>
        + ArgminL2Norm<F>
        + SerializeAlias
        + DeserializeOwnedAlias,
    G: Clone
        + std::fmt::Debug
        + ArgminMul<F, P>
        + ArgminSub<G, G>
        + ArgminL2Norm<F>
        + SerializeAlias
        + DeserializeOwnedAlias,
    F: ArgminFloat,
{
    const NAME: &'static str = "Nesterov";

    fn init(
        &mut self,
        _problem: &mut Problem<O>,
        state: IterState<P, G, (), (), F>,
    ) -> Result<(IterState<P, G, (), (), F>, Option<KV>), Error> {
        self.step_length = Some(self.initial_step_length);
        self.y = state.param.clone();
        Ok((state, None))
    }

    fn next_iter(
        &mut self,
        problem: &mut Problem<O>,
        mut state: IterState<P, G, (), (), F>,
    ) -> Result<(IterState<P, G, (), (), F>, Option<KV>), Error> {
        let x_prev = state.take_prev_param();

        let y = self.y.take().ok_or_else(argmin_error_closure!(
            NotInitialized,
            "y is not initialized"
        ))?;

        // Obtain current parameter vector.
        let x = state
            .get_param()
            .ok_or_else(argmin_error_closure!(
                NotInitialized,
                "Initial parameter vector required!"
            ))?
            .clone();

        // Compute the gradient.
        let grad = problem.gradient(&x)?;

        // Use 1/L as initial step length, where L is the estimated Lipschitz constant.
        let step_length_estimate = |x_prev: &P, x: &P, grad_prev: &G, grad: &G| -> F {
            x.sub(&x_prev).l2_norm() / grad.sub(&grad_prev).l2_norm()
        };

        // Find step length with back-tracking.
        let (step_length, y_next) = {
            // Use 1/L as initial step length, where L is the estimated Lipschitz constant.
            let mut step_length_new = if let Some(grad_prev) = state.get_prev_gradient() {
                // Get the previous gradient.
                let x_prev = x_prev.ok_or_else(argmin_error_closure!(
                    PotentialBug,
                    "previous parameter is not present but previous gradient is"
                ))?;
                step_length_estimate(&x_prev, &x, grad_prev, &grad)
            } else {
                // Start with a small step length.
                self.initial_step_length
            };
            debug_assert!(step_length_new.is_finite());

            let epsilon = float!(0.95);

            let step_length = self.step_length.ok_or_else(argmin_error_closure!(
                PotentialBug,
                "step length is not initialized"
            ))?;

            let step_length_next =
                (F::one() + F::sqrt(F::one() + float!(4.0) * step_length.powi(2))) / float!(2.);
            debug_assert!(step_length_next.is_finite());

            let mut count = 10;
            loop {
                // y_next = x_prev - step_size * gradient(x_prev)
                let y_next = x.scaled_sub(&step_length, &grad);

                // x_next = (1-gamma_prev)*y_next + gamma_prev * y_prev
                let weight = (step_length - F::one()) / step_length_next;
                let y_diff = y_next.sub(&y);
                let x_next = y_next.scaled_add(&weight, &y_diff);

                let grad_new = problem.gradient(&x_next)?;

                let step_length_ref = step_length_estimate(&x, &x_next, &grad, &grad_new);
                debug_assert!(step_length_ref.is_finite());

                if step_length_new * epsilon < step_length_ref {
                    // Step length is good enough.
                    break (step_length_new, y_next);
                } else {
                    // Do a better estimate of the step length based on the new gradient.
                    step_length_new = step_length_ref;
                }

                count -= 1;
                if count == 0 {
                    todo!();
                }
            }
        };

        let step_length_next =
            (F::one() + F::sqrt(F::one() + float!(4.0) * step_length.powi(2))) / float!(2.);

        let weight = (step_length - F::one()) / step_length_next;
        let y_diff = y_next.sub(&y);
        let x_next = y_next.scaled_add(&weight, &y_diff);

        //self.linesearch.search_direction(gradient.mul(&float!(-1.)));
        //self.linesearch.initial_step_length(step_length)?;

        //let OptimizationResult {
        //    problem: line_problem,
        //    state: mut linesearch_state,
        //    ..
        //} = Executor::new(
        //    problem.take_problem().ok_or_else(argmin_error_closure!(
        //        PotentialBug,
        //        "`Nesterov`: Failed to take `problem` for line search"
        //    ))?,
        //    self.linesearch.clone(),
        //)
        //.configure(|config| config.param(x).gradient(gradient))
        //.ctrlc(false)
        //.run()?;

        //// Retrieve the problem including function evaluation counts.
        //problem.consume_problem(line_problem);

        //// Retrieve step length a_k.
        //let step_length = todo!();

        //// Retrieve parameter found by line search.
        //let y_next = linesearch_state
        //    .take_param()
        //    .ok_or_else(argmin_error_closure!(
        //        PotentialBug,
        //        "no `param` returned by line search"
        //    ))?;

        // Update state.
        self.y = Some(y_next);

        self.step_length = Some(step_length_next);

        let mut state = state
            // Set new parameter, and shift present one to `prev_param`.
            .param(x_next);
        assert!(state.prev_param.is_some());
        state.prev_grad = Some(grad);

        // Return the new state.
        Ok((state, None))
    }

    fn terminate(&mut self, state: &IterState<P, G, (), (), F>) -> argmin::core::TerminationStatus {
        if let Some(grad) = &state.prev_grad {
            let norm = grad.l2_norm();
            if norm < self.target_gradient_l2_norm {
                TerminationStatus::Terminated(TerminationReason::SolverConverged)
            } else {
                TerminationStatus::NotTerminated
            }
        } else {
            TerminationStatus::NotTerminated
        }
    }
}

#[test]
fn test_nesterov_gradient_descent() {
    use argmin::core::CostFunction;

    let solver = Nesterov::new();

    struct Op;

    impl CostFunction for Op {
        type Param = f64;

        type Output = f64;

        fn cost(&self, param: &Self::Param) -> Result<Self::Output, Error> {
            Ok((param - 1.0).powi(2))
        }
    }

    impl Gradient for Op {
        type Param = f64;

        type Gradient = f64;

        fn gradient(&self, param: &Self::Param) -> Result<Self::Gradient, Error> {
            Ok(-2. * (1. - param))
        }
    }

    let result = argmin::core::Executor::new(Op, solver)
        .configure(|state| state.param(2.).max_iters(100).target_cost(0.))
        .run()
        .expect("optimization failed");

    let state = result.state();
    dbg!(state);
    assert_eq!(
        state.get_termination_status(),
        &TerminationStatus::Terminated(TerminationReason::SolverConverged)
    );
    assert!(state.get_best_param().is_some());
    assert!((state.get_best_param().unwrap() - 1.0).abs() <= 1e-6);
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct BkTrk<P, F> {
    search_direction: Option<P>,
    step_length: Option<F>,
}

impl<P, F> BkTrk<P, F>
where
    F: Copy,
{
    pub fn new() -> Self {
        Self {
            search_direction: None,
            step_length: None,
        }
    }

    /// Get current step length, if any.
    fn step_length(&self) -> Option<F> {
        self.step_length
    }
}

impl<P, F> LineSearch<P, F> for BkTrk<P, F>
where
    F: ArgminFloat,
{
    fn search_direction(&mut self, direction: P) {
        self.search_direction = Some(direction);
    }

    fn initial_step_length(&mut self, step_length: F) -> Result<(), argmin::core::Error> {
        if step_length <= F::zero() {
            return Err(argmin_error!(
                PotentialBug,
                "initial step length must be > 0."
            ));
        }
        self.step_length = Some(step_length);
        Ok(())
    }
}

impl<O, F, P, G> Solver<O, IterState<P, G, (), (), F>> for BkTrk<P, F>
where
    F: ArgminFloat,
    O: Gradient<Param = P, Gradient = G>,
    P: Clone
        + ArgminScaledSub<G, F, P>
        + ArgminScaledAdd<P, F, P>
        + ArgminScaledAdd<G, F, P>
        + ArgminMul<F, P>
        + ArgminSub<P, P>
        + ArgminL2Norm<F>
        + SerializeAlias
        + DeserializeOwnedAlias,
    G: ArgminMul<F, P> + ArgminSub<G, G> + ArgminL2Norm<F> + SerializeAlias + DeserializeOwnedAlias,
{
    const NAME: &'static str = "BkTrk";

    fn next_iter(
        &mut self,
        problem: &mut Problem<O>,
        mut state: IterState<P, G, (), (), F>,
    ) -> Result<(IterState<P, G, (), (), F>, Option<KV>), Error> {
        let vk = state
            .take_param()
            .ok_or_else(argmin_error_closure!(PotentialBug, "param not present"))?;
        let vk_prev = state.take_prev_param().ok_or_else(argmin_error_closure!(
            PotentialBug,
            "prev param not present"
        ))?;
        let grad = state
            .take_gradient()
            .ok_or_else(argmin_error_closure!(PotentialBug, "gradient not present"))?;
        let grad_prev = state
            .take_prev_gradient()
            .ok_or_else(argmin_error_closure!(
                PotentialBug,
                "prev gradient not present"
            ))?;

        // Use 1/L as initial step length, where L is the estimated Lipschitz constant.
        let step_length_estimate = vk.sub(&vk_prev).l2_norm() / grad.sub(&grad_prev).l2_norm();

        let u_hat_next = vk.scaled_sub(&step_length_estimate, &grad);
        todo!()
    }
}
