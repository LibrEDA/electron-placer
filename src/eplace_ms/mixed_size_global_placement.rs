// Copyright (c) 2019-2020 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Global placement engine. Finds positions of macros and standard-cells such that
//! the wire length is optimized and density constraints are met with little overlaps.
//!
//! Cells and macros do not get legalized.
//! After global placement the macros should be legalized (for example with the `AnnealingMacroLegalizer`.
//! During macro legalization the standard-cells are intended to be a soft-blockage: Overlap with them
//! should be minimized but is allowed.
//! After macro legalization the macros should be fixed and the standard-cells should be
//! placed again (using the current location as initial positions) and then legalized.

use argmin::core::Gradient;
use argmin_math::ArgminL2Norm;
use fnv::FnvHashMap;

use db::traits::*;
use libreda_pnr::db::SimpleTransform;
use libreda_pnr::db::{self, IntoEdges};
use libreda_pnr::metrics::placement_density::DensityMap;
use libreda_pnr::place::mixed_size_placer::{MixedSizePlacer, PlacementError};
use rayon::prelude::{
    IndexedParallelIterator, IntoParallelRefIterator, IntoParallelRefMutIterator, ParallelIterator,
};

use std::collections::{HashMap, HashSet};

use super::argmin_types::*;
use super::efield::*;
use super::half_perimeter_wire_length::log_sum_exp_wire_length_gradient;
use crate::eplace_ms::half_perimeter_wire_length::{
    log_sum_exp_wire_length, log_sum_exp_wire_length_second_order_gradient,
};
use crate::kahan_sum::*;

// crates.io
use itertools::Itertools;
use libreda_pnr::place::placement_problem::PlacementProblem;
use log;
use num_traits::{FromPrimitive, Zero};
use rand::prelude::*;
use rand_pcg;
use rand_pcg::Pcg64;
use rustfft::num_complex::Complex;

/// Global mixed-size placement engine.
pub struct MixedSizeGlobalPlacer {
    /// Target placement density. Must be larger than zero and should be smaller than one.
    target_density: f64,
    /// Maximal number of placement iterations.
    max_iter: usize,
    /// FFT resolution. Approximate number of pixels per standard-cell.
    pixel_per_cell: f64,
    /// A value between `0.0` and `1.0`. Default is `0.0`.
    /// This is used as the initial weight of the density penalty in the optimization loop.
    /// A low value will favour the optimization for wire-length at the beginning.
    /// A high value will skip the optimization for wire-length and directly optimize the density constraints.
    /// A high value can be used if it the wire-length is known to be already good. For example after buffer insertion.
    initial_density_penalty: f64,
}

impl MixedSizeGlobalPlacer {
    /// * `target_density`: Target placement density. Must be larger than zero and should be smaller than one.
    pub fn new(target_density: f64) -> Self {
        assert!(target_density > 0., "Target density must be larger than 0.");
        if target_density > 1. {
            // Warn about high target density.
            log::warn!("Target density is larger than 1: {}", target_density);
        }
        Self {
            target_density,
            max_iter: 200,
            pixel_per_cell: 1.0,
            initial_density_penalty: 0.0,
        }
    }

    /// Set maximum number of iterations.
    pub fn max_iter(&mut self, max_iter: usize) {
        self.max_iter = max_iter;
    }

    /// Set the initial balance between wire-length cost and density penalty.
    ///
    /// Use this for 'incremental' placements when the given placement is known to be already good
    /// in wire-length. This can be useful for example after minor modifications to the netlist such as buffer insertion
    /// or tie-cell insertion.
    ///
    /// Must be value between `0.0` and `1.0`. Default is `0.0`.
    ///
    /// This is used as the initial weight of the density penalty in the optimization loop.
    /// A low value will favour the optimization for wire-length at the beginning.
    /// A high value will skip the optimization for wire-length and directly optimize the density constraints.
    /// A high value can be used if it the wire-length is known to be already good. For example after buffer insertion.
    ///
    /// # Panics
    /// Panics if the value is out of the required range `[0.0..1.0]`.
    pub fn initial_density_penalty(&mut self, initial_density_penalty: f64) {
        assert!(initial_density_penalty >= 0.0);
        assert!(initial_density_penalty <= 1.0);
        self.initial_density_penalty = initial_density_penalty
    }

    /// Set the relative FFT resolution in number of pixels per standard-cell (approximately).
    pub fn pixel_per_cell(&mut self, pixel_per_cell: f64) {
        assert!(pixel_per_cell > 0.0);
        self.pixel_per_cell = pixel_per_cell;
    }
}

/// For debugging.
/// Write a 2D array into a CSV file.
#[allow(unused)]
fn dump_array2<T: std::fmt::Display>(path: &str, v: &ndarray::Array2<T>) {
    //use std::io::Write;
    //log::info!("Write density map to {}.", path);
    //let mut f = std::fs::File::create(path).unwrap();

    //let (w, h) = v.dim();

    //for j in 0..h {
    //    for i in 0..w {
    //        write!(f, "{:}", v[[i, j]]);
    //        if i + 1 < w {
    //            write!(f, ", ");
    //        }
    //    }
    //    write!(f, "\n");
    //}
}

struct PlacementInstance<'a, C>
where
    C: db::L2NBase<Coord = db::Coord>,
{
    placer: &'a MixedSizeGlobalPlacer,
    placement_problem: &'a dyn PlacementProblem<C>,
    cell_outlines: FnvHashMap<C::CellId, db::Rect<db::Coord>>,
    net_weights: FnvHashMap<C::NetId, f64>,

    /// Cell instances which can be moved during  placement.
    movable_instances: HashSet<C::CellInstId>,
    /// Cell instances with fixed locations.
    fixed_instances: HashSet<C::CellInstId>,
    /// Union of movable and fixed cell instances.
    all_instances: HashSet<C::CellInstId>,

    core_area_bbox: db::Rect<i32>,
    /// Regions where cells can be placed.
    /// Non-overlapping rectangles.
    placement_sites: Vec<db::Rect<i32>>,
    /// Regions which should not be used for placement (but can be used).
    soft_blockages: Vec<db::Rect<i32>>,
}

impl<'a, C> PlacementInstance<'a, C>
where
    C: db::L2NBase<Coord = db::Coord>,
{
    fn new(
        placer: &'a MixedSizeGlobalPlacer,
        placement_problem: &'a dyn PlacementProblem<C>,
    ) -> Result<Self, PlacementError> {
        let placement_sites = placement_problem.placement_region();
        // Determine the size of the density raster.
        let core_area_bbox = placement_sites
            .iter()
            .try_into_bounding_box()
            .ok_or_else(|| PlacementError::Other("Core area cannot be empty.".to_string()))?;

        // Grow the box to each side by 50%. This is needed to have a margin for
        // aliasing effects during the computation of the cyclic convolution.
        // (otherwise a electric force from the left edge would affect a cell on the right edge.)
        let fft_boundary: db::Rect<i32> =
            core_area_bbox.sized(core_area_bbox.width() / 2, core_area_bbox.height() / 2);
        log::debug!("Core size: {:?}", core_area_bbox);
        log::debug!("FFT region size: {:?}", fft_boundary);

        let movable_instances: HashSet<C::CellInstId> = placement_problem.get_movable_instances();
        let fixed_instances: HashSet<C::CellInstId> = placement_problem.get_fixed_instances();
        // Get the set of all instances relevant to this placement run.
        let all_instances: HashSet<_> =
            movable_instances.union(&fixed_instances).cloned().collect();

        let mut inst = Self {
            placer,
            placement_problem,
            cell_outlines: Default::default(),
            net_weights: Default::default(),
            movable_instances,
            fixed_instances,
            all_instances,
            core_area_bbox,
            placement_sites: Default::default(),
            soft_blockages: Default::default(),
        };

        inst.init();

        Ok(inst)
    }

    /// Grow the bounding-box of the core area to each side by 50%. This is needed to have a margin for
    /// aliasing effects during the computation of the cyclic convolution.
    /// (otherwise a electric force from the left edge would affect a cell on the right edge.)
    fn fft_boundary(&self) -> db::Rect<i32> {
        self.core_area_bbox.sized(
            self.core_area_bbox.width() / 2,
            self.core_area_bbox.height() / 2,
        )
    }

    /// Rasterized data structure for representing cell densities.
    fn create_fft_kernel(&self) -> EFieldKernel<f64> {
        EFieldKernel::new(self.fft_boundary().cast(), self.compute_raster_resolution())
    }

    fn init(&mut self) {
        let chip = self.placement_problem.fused_layout_netlist();
        let top_cell = self.placement_problem.top_cell();
        let top_ref = chip.cell_ref(&top_cell);

        self.cell_outlines = chip
            .each_cell()
            .flat_map(|cell_id| {
                self.placement_problem
                    .cell_outline(&cell_id)
                    .map(|outline| (cell_id, outline))
            })
            .collect();

        // Fetch all net weights.
        self.net_weights = top_ref
            .each_net()
            .map(|net| (net.id(), self.placement_problem.net_weight(&net.id())))
            .collect();

        self.placement_sites = self.init_placement_sites();
        self.soft_blockages = self.init_soft_blockages();
    }

    /// Get the placement blockages as a list of non-overlapping rectangles.
    fn init_obstructions(&self) -> Vec<db::Rect<i32>> {
        let chip = self.placement_problem.fused_layout_netlist();
        let top_cell = self.placement_problem.top_cell();
        let top_ref = chip.cell_ref(&top_cell);

        // Create obstructions from fixed cells.
        let obstructions = top_ref
            .each_cell_instance()
            .filter(|inst| self.fixed_instances.contains(&inst.id())) // Skip movable instances.
            .flat_map(|inst| {
                // ignore the cells that don't have an outline.
                let bbox = self.cell_outlines.get(&inst.template_id());

                // Move bbox to actual location.
                bbox.map(|bbox| {
                    let pos = self.placement_problem.initial_position(&inst.id());
                    //.unwrap_or_else(|| chip.get_transform(&inst.id()));
                    bbox.transform(|p| pos.transform_point(p))
                })
            });

        let edges = obstructions
            .flat_map(|r| r.into_edges())
            .map(|redge| redge.into()); // Convert into general non-rectilinear edge.

        let merged_obstructions = iron_shapes_booleanop::edges_boolean_op(
            iron_shapes_booleanop::edge_intersection_integer,
            edges,
            std::iter::empty(),
            iron_shapes_booleanop::Operation::Union,
            iron_shapes_booleanop::PolygonSemantics::Union,
        );

        // Convert merged polygon in to set of rectilinear edges.
        let edges = merged_obstructions
            .all_edges_iter()
            .map(|edge| db::REdge::try_from(&edge).expect("found non-rectilinear edge"));

        // Compute boolean union placement blockages and
        // decompose it into non-overlapping rectangles.
        iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles(edges)
    }

    fn init_soft_blockages(&self) -> Vec<db::Rect<i32>> {
        let edges = self
            .placement_problem
            .soft_blockages()
            .into_iter()
            .flat_map(|rpolygon| rpolygon.into_edges());

        // Compute boolean union placement blockages and
        // decompose it into non-overlapping rectangles.
        iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles(edges)
    }

    /// Get the regions where cells can be placed as a list of non-overlapping rectangles.
    fn init_placement_sites(&self) -> Vec<db::Rect<i32>> {
        let placement_sites_raw = self.placement_problem.placement_region();
        let placement_sites_edges = placement_sites_raw.iter().flat_map(|s| s.edges());
        let obstructions = self.init_obstructions();
        let obstructions_edges = obstructions.iter().flat_map(|r| r.into_edges());

        let subject = placement_sites_edges.map(|e| e.into());
        let clipping = obstructions_edges.map(|e| e.into());

        // Subtract obstructions.
        let placement_sites_without_obstructions = iron_shapes_booleanop::edges_boolean_op(
            iron_shapes_booleanop::edge_intersection_integer,
            subject,
            clipping,
            iron_shapes_booleanop::Operation::Difference,
            iron_shapes_booleanop::PolygonSemantics::Union,
        );

        let merged_edges = placement_sites_without_obstructions
            .all_edges_iter()
            .map(|e| db::REdge::try_from(&e).expect("edge is not rectilinear"));

        // Decompose into non-overlapping rectangles.
        iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles(merged_edges)
    }

    /// Convert all cell instances into a list of `Component` datastructures.
    fn create_components(&self) -> Vec<Component<C::CellInstId>> {
        let chip = self.placement_problem.fused_layout_netlist();
        let top_cell = self.placement_problem.top_cell();
        let top_ref = chip.cell_ref(&top_cell);

        let initial_positions: HashMap<_, _> = self
            .movable_instances
            .union(&self.fixed_instances)
            .cloned()
            .map(|inst| {
                let pos = self.placement_problem.initial_position(&inst);
                (inst, pos)
            })
            .collect();

        let mut rng = Pcg64::new(42, 0xa02bdbf7bb3c0a7ac28fa16a64abf96);

        // Create a list of all cells.
        let mut components = Vec::new();
        for cell in top_ref.each_cell_instance() {
            let is_movable = self.movable_instances.contains(&cell.id());
            let is_fixed = self.fixed_instances.contains(&cell.id());

            if !(is_movable || is_fixed) {
                // Ingore cell.
                log::warn!(
                    "Ignore cell during placement (not marked as movable nor fixed): {}",
                    cell.qname("/")
                );
                continue;
            }

            // Find cell size.
            let outline_int: db::Rect<C::Coord> = {
                if let Some(outline) = self.cell_outlines.get(&cell.template_id()) {
                    *outline
                } else {
                    log::debug!("No outline defined for cell '{}'.", cell.template().name());

                    db::Rect::new(db::Point::zero(), db::Point::zero())
                }
            };
            let outline: db::Rect<f64> = outline_int.cast();

            // Get initial position.
            let position = initial_positions
                .get(&cell.id())
                .cloned()
                .unwrap_or_else(|| chip.get_transform(&cell.id()));

            let position = if !is_fixed {
                // Apply a slight pseudo-random perturbation to break instable balances
                // that could come from the initial placement. (For example when two cells
                // with same shape perfectly overlap, or some cells are perfectly arranged in one
                // row and no force is driving them apart.)

                let perturbation: db::Vector<C::Coord> = {
                    let w = outline_int.width();
                    let h = outline_int.height();
                    let div = C::Coord::from_u32(8).unwrap();
                    let dx = w / div;
                    let dy = h / div;

                    db::Vector::new(rng.gen_range(-dx..dx), rng.gen_range(-dy..dy))
                };

                db::SimpleTransform {
                    displacement: position.displacement + perturbation,
                    ..position
                }
            } else {
                position
            };

            // Set the charge density equal to the target density for macro blocks.
            let (outline, charge_density) = {
                // A block that spans for sure a full bin and more is considered a large macro.
                let (bin_w, bin_h) = {
                    let (nx, ny) = self.compute_raster_resolution();
                    let bbox = self.fft_boundary();
                    let bin_width = bbox.width() as f64 / f64::from_usize(nx).unwrap();
                    let bin_height = bbox.height() as f64 / f64::from_usize(ny).unwrap();
                    (bin_width, bin_height)
                };

                let is_large_block = outline.width() > 2. * bin_w && outline.height() > 2. * bin_h;
                if is_large_block {
                    // let w = outline.width();
                    // let h = outline.height();
                    // let shrink_factor = 1. - (1.0 / self.target_density).sqrt();
                    // let outline_adapted = outline.sized(-w * shrink_factor / 2., -h * shrink_factor / 2.);
                    // // let charge_density = self.target_density * w * h /
                    // //     outline_adapted.width() * outline_adapted.height();
                    // (outline_adapted, 1.0)
                    (outline, self.placer.target_density)
                } else {
                    (outline, 1.0)
                }
            };

            let component = Component {
                id: Some(cell.id()),
                size: outline,
                charge_density,
                position: position.cast(),
                is_fixed,
                is_filler: false,
            };

            components.push(component);
        }

        // Add top cell.
        // Adding the top cell is required to respect its pin shapes (if any) for the placement.
        components.push(Component {
            id: None, // None -> this is not a cell instance but the top cell.
            size: db::Rect::new(db::Point::zero(), db::Point::zero()),
            charge_density: 0.0, // Top cell is a virtual component and does not block its sub cells.
            position: SimpleTransform::identity(),
            is_fixed: true,
            is_filler: false,
        });

        components
    }

    /// Find required raster resolution to resolve the cells and filler cells.
    /// That is a rule of thumb.
    fn compute_raster_resolution(&self) -> (usize, usize) {
        let smallest_cell_size: db::Rect<f64> = self
            .cell_outlines
            .values()
            .min_by_key(|r| r.width() as usize * r.height() as usize)
            .expect("No cell outlines given!")
            .cast();

        let bbox_area: f64 = self.core_area_bbox.cast().area_doubled_oriented();

        // TODO: Use median/average cell size.
        let required_num_pixels = 0.25 * self.placer.pixel_per_cell * bbox_area
            / smallest_cell_size.area_doubled_oriented();

        // Required raster side length for a square raster.
        let n_square = required_num_pixels.sqrt();

        // Adjust to the aspect ratio of the core bounding box.
        let (w, h) = (
            self.core_area_bbox.width() as f64,
            self.core_area_bbox.height() as f64,
        );
        // Scale by 2 because the effective raster is twice as big to avoid aliasing effects.
        let nx = n_square * w / (w + h) * 2.;
        let ny = n_square * h / (w + h) * 2.;

        //log::debug!("FFT raster size (not rounded): {}x{}", nx, ny);

        // Round up to next power of 2 to accelerate the FFT.
        // TODO: Find the optimal size for FFT (many small factors, etc...).
        let nx = (nx as usize)
            .checked_next_power_of_two()
            .expect("Failed to get next power of two.");
        let ny = (ny as usize)
            .checked_next_power_of_two()
            .expect("Failed to get next power of two.");

        //log::debug!("FFT raster size (rounded up): {}x{}", nx, ny);

        (nx, ny)
    }

    /// Find all cell areas and report cells which have no defined outlines.
    fn report_cells_without_outline(&self) {
        let chip = self.placement_problem.fused_layout_netlist();
        let top_cell = self.placement_problem.top_cell();
        let top_ref = chip.cell_ref(&top_cell);

        let mut cells_without_outlines = HashSet::new();
        let total_cell_area: f64 = {
            let areas = top_ref
                .each_cell_instance()
                .filter(|inst| self.all_instances.contains(&inst.id())) // Ignore the non-relevant instances.
                .map(|inst| {
                    let area = self.cell_outlines.get(&inst.template_id()).map(|r| {
                        let r: db::Rect<f64> = r.cast();
                        r.area_doubled_oriented() / 2.
                    });
                    if area.is_none() {
                        cells_without_outlines.insert(inst.template().name());
                    }
                    area.unwrap_or(0.)
                });
            areas.kahan_sum()
        };

        // Print a warning if there are cells without defined outline.
        if !cells_without_outlines.is_empty() {
            log::warn!(
                "No outline defined for cells: {}",
                cells_without_outlines.iter().sorted().join(", ")
            );
        }

        // Lower bound for placement density.
        let core_area_usage = total_cell_area / self.available_core_area();
        if core_area_usage > self.placer.target_density {
            log::warn!(
                "Core area usage is larger than target density: target density = {}",
                self.placer.target_density
            );
        }
        log::info!("Core area usage: {:0.4}%", core_area_usage * 100.);
    }

    /// Total area of placement sites.
    fn available_core_area(&self) -> f64 {
        self.placement_sites
            .iter()
            .map(|r| r.width() as u64 * r.height() as u64)
            .sum::<u64>() as f64
    }

    /// Create a data structure which represents the optimization problem.
    fn create_optimization_problem(&self) -> OptimizationProblem<C> {
        let chip = self.placement_problem.fused_layout_netlist();
        let top_cell = self.placement_problem.top_cell();
        let top_ref = chip.cell_ref(&top_cell);

        let fft_kernel = self.create_fft_kernel();

        // Create a list of all cells.
        let mut components = self.create_components();
        let mut num_filler_cells = 0;

        // Create filler cells.
        let use_filler_cells = true;
        if use_filler_cells {
            let fillers = {
                let mut density = fft_kernel.new_density_map();
                self.init_canvas(&mut density);
                draw_components(&mut density, &components);
                self.create_filler_cells(&density.charge_distribution)
            };

            num_filler_cells = fillers.len();
            components.extend_from_slice(&fillers);
        }

        // Get the indices of all components which are movable.
        let movable_components: Vec<usize> = components
            .iter()
            .enumerate()
            .filter_map(|(idx, component)| (!component.is_fixed).then_some(idx))
            .collect();

        let component_indices_by_net = self.create_net2component_lut(top_ref, &components);

        // Sanity check:
        debug_assert!(
            component_indices_by_net
                .iter_nets()
                .map(|(net, _)| net)
                .unique()
                .count()
                == component_indices_by_net.num_nets(),
            "a net must be present no more than once"
        );

        let mut default_density = fft_kernel.new_density_map();
        self.init_canvas(&mut default_density);

        OptimizationProblem {
            boundary: self.core_area_bbox.cast(),
            fft_boundary: self.fft_boundary().cast(),
            fft_kernel,
            components,
            num_filler_cells,
            movable_components,
            default_density,
            net_weights: &self.net_weights,
            component_indices_by_net,
            placement_sites: self.placement_sites.clone(),
            target_density: self.placer.target_density,
        }
    }

    /// Clear the canvas and draw the region where cells should be placed.
    /// The canvas represents a density requirement map: Where no cells should be placed
    /// the value is zero, where cells should be placed with density `d` the value is `-d`.
    fn init_canvas(&self, density: &mut EDensity<f64>) {
        let target_density = self.placer.target_density;
        let placement_sites = &self.placement_sites;
        debug_assert!(target_density > 0.);
        density.clear();

        for r in placement_sites {
            density.draw_charge_density(&r.cast(), -target_density);
        }
    }

    /// Create a lookup-table which maps net IDs to a list of component indices of
    /// all components which are attached to this net.
    /// Additional to the component index also stores the relative position of the pin.
    fn create_net2component_lut(
        &self,
        top_cell: CellRef<C>,
        components: &[Component<C::CellInstId>],
    ) -> AdjacencyList<C::NetId>
    where
        C: db::L2NBase<Coord = db::Coord>,
    {
        // Find simplified pin locations of all the cells.
        let pin_locations = extract_pin_locations(top_cell.base());

        // Create lookup tables for translating between indices and IDs.
        let component_by_id: FnvHashMap<_, _> = components
            .iter()
            .map(|c: &Component<_>| c.id.clone())
            .enumerate()
            .map(|(i, c)| (c, ComponentIdx(i)))
            .collect();

        // For each net find all components that are connected to it
        // and also store the location of the pin relative to the component.
        top_cell
            .each_net()
            .fold(AdjacencyList::new(), |mut adj, net| {
                // Find all cells connected to this net.
                let connected_cell_instances = net
                    .each_terminal()
                    .filter_map(|t| {
                        let (cell_inst, pin) = match t {
                            TerminalRef::Pin(p) => (None, p),
                            TerminalRef::PinInst(i) => {
                                // Find location of the pin instance relative to the cell instance.
                                let pin = i.pin();
                                (Some(i.cell_instance()), pin)
                            }
                        };

                        if let Some(cell_inst) = &cell_inst {
                            // Ignore non-relevant cell instances.
                            if !self.all_instances.contains(&cell_inst.id()) {
                                return None;
                            }
                        }

                        // Find location of pin.
                        let cell = pin.cell();
                        let pin_location = pin_locations
                            .get(&cell.id())
                            .and_then(|l| l.get(&pin.id()))
                            .copied()
                            .unwrap_or({
                                // Take the center of the cell as pin location if no pin shapes are defined.
                                self.cell_outlines
                                    .get(&cell.id())
                                    .map(|outline| outline.center())
                                    .unwrap_or((0, 0).into()) // Take (0, 0) as last resort.
                            })
                            .cast();

                        Some((cell_inst.map(|i| i.id()), pin_location))
                    })
                    // Convert to component indices.
                    .map(|(id, pin_loc)| ComponentPin {
                        component_idx: component_by_id[&id],
                        relative_location: pin_loc,
                    });

                adj.add_net(net.id(), connected_cell_instances);

                adj
            })
    }

    /// Determine a good size and charge density for filler cells.
    fn filler_cell_size(&self) -> (f64, db::Rect<i32>) {
        let netlist = self.placement_problem.fused_layout_netlist();

        let filler_cell_size = self
            .movable_instances
            .iter()
            .filter_map(|cell_inst_id| {
                let cell_id = netlist.template_cell(cell_inst_id);
                self.cell_outlines.get(&cell_id)
            })
            // Skip zero-sized cells.
            .filter(|r| r.width() > 0 && r.height() > 0)
            .min_by_key(|r| r.width() as u64 * r.height() as u64)
            .expect("No cell outlines given!");

        //// The cell sizes which are used in the design and count how many times they are being used.
        //let counts = self
        //    .movable_instances
        //    .iter()
        //    .filter_map(|cell_inst_id| {
        //        let cell_id = netlist.template_cell(cell_inst_id);
        //        self.cell_outlines.get(&cell_id)
        //    })
        //    .counts()
        //    .into_iter()
        //    .sorted_by_key(|(_, count)| *count)
        //    .collect_vec();

        //let total: usize = counts.iter().map(|(_, count)| count).sum();

        let original_filler_area =
            filler_cell_size.width() as f64 * filler_cell_size.height() as f64;
        let original_filler_density = 1.0;

        let filler_density: f64 = 1. / 16.;
        assert!((0. ..=1.).contains(&filler_density));

        let filler_charge = original_filler_density * original_filler_area;

        // Enlarge fillers such that their charge is preserved.
        let filler_magnification = (1. / filler_density).sqrt();
        assert!(1. <= filler_magnification);

        let s = filler_cell_size.width().max(filler_cell_size.height());
        let s = (s as f64 * filler_magnification) as i32;
        let filler_cell_size = db::Rect::new((0, 0), (s, s));

        let filler_area = filler_cell_size.width() as f64 * filler_cell_size.height() as f64;

        let filler_density = filler_charge / filler_area;

        log::debug!(
            "Size of filler cells: {}x{}",
            filler_cell_size.width(),
            filler_cell_size.height()
        );

        log::debug!("filler charge density: {}", filler_density);

        (filler_density, filler_cell_size)
    }

    /// Create a list of filler cells which balance the negatively charged bins
    /// and bring the net charge to zero (if possible).
    /// # Panics
    /// Panics if the net charge is positive, i.e. the total cell area
    /// is larger than `available core area` * `target density`.
    fn create_filler_cells<ComponentId>(
        &self,
        density_bins: &DensityMap<f64, Complex<f64>>,
    ) -> Vec<Component<ComponentId>> {
        // TODO: Determine size of filler cells (e.g. average std-cell size, smallest std-cell size, maybe also have different sizes, ...).
        // ... Filler size = Size of a pixel?
        // The total area of filler cells equals the empty core area.

        let (filler_charge_density, filler_cell_size) = self.filler_cell_size();

        let num_filler_cells = {
            let filler_area = filler_cell_size.width() as f64 * filler_cell_size.height() as f64;
            let filler_charge = filler_area * filler_charge_density;
            assert!(filler_area > 0., "fillers must have a non-zero area");
            assert!(filler_charge > 0., "fillers must have a non-zero charge");

            let total_charge: f64 = density_bins.get_data_ref().iter().map(|v| v.re).kahan_sum();

            assert!(
                total_charge <= 0.,
                "cannot compensate over-utilization by inserting filler cells"
            );

            // Compute number of required filler cells to fill the unused chip area.
            -total_charge / filler_charge
        };

        assert!(
            num_filler_cells >= 0.,
            "cannot insert a negative number of filler cells"
        );

        let num_filler_cells_floor = num_filler_cells as usize;

        log::debug!("Number of filler cells: {}", num_filler_cells_floor);

        // Find initial filler locations.
        // Put fillers such that average density will be zero.
        // Place fillers pseudo-randomly with a distribution equal to the
        // negative density.
        let seed = 42;
        let mut rng = rand_pcg::Pcg32::new(seed, 7);

        // Find bins with negative density (they need filler cells).
        // Sort them ascending by abs(density).
        let negative_densities: Vec<_> = density_bins
            .get_data_ref()
            .indexed_iter()
            // Select negative densities.
            .filter_map(|(idx, density)| (density.re < 0.).then_some((idx, -density.re)))
            // Sort them ascending by magnitute.
            .sorted_by(|(_idx1, density1), (_idx2, density2)| density1.total_cmp(density2))
            .collect();

        if negative_densities.is_empty() {
            // No fillers needed.
            return vec![];
        }

        // Extract the density values.
        // They serve as weights for the probability distribution used
        // to place the filler cells.
        let bin_weights: Vec<_> = negative_densities.iter().map(|(_, v)| *v).collect();

        // Create statistic distribution weighted with the bin densities.
        let dist = rand::distributions::WeightedIndex::new(bin_weights).expect("Invalid weights");

        let (pixel_width, pixel_height) = density_bins.bin_dimension();

        // Create filler cells.
        (0..num_filler_cells_floor + 1)
            .map(|i| {
                // Pick random bin based on weights.
                let (bin_index, _) = negative_densities[dist.sample(&mut rng)];
                // Convert bin index to position.
                let bin_location = density_bins.bin_center(bin_index);

                // Make sure that the fillers don't have the same position.
                // They would never move away from each other.
                let random_displacement = db::Point::new(
                    rng.gen_range(-pixel_width / 2.0..pixel_width / 2.0),
                    rng.gen_range(-pixel_height / 2.0..pixel_height / 2.0),
                );

                let filler_position =
                    bin_location + random_displacement - filler_cell_size.center().cast();

                let charge_density = if i == num_filler_cells_floor {
                    // Last filler cell has less charge than the others.
                    filler_charge_density * num_filler_cells.fract()
                } else {
                    filler_charge_density
                };

                // Filler cell.
                Component {
                    id: None,
                    size: filler_cell_size.cast(),
                    position: db::SimpleTransform::translate(filler_position),
                    is_fixed: false,
                    is_filler: true,
                    charge_density,
                }
            })
            .collect()
    }
}

/// Randomly perturb positions of movable cells by a small amount.
fn perturb_positions<C: db::NetlistIds>(
    op: &OptimizationProblem<'_, C>,
    positions: &mut [db::Vector<f64>],
    seed: u64,
) {
    let mut rng = Pcg64::new(seed as u128, 0xa02bdbf7bb3c0a7ac28fa16a64abf96);

    for (&c, pos) in op.movable_components.iter().zip_eq(positions.iter_mut()) {
        debug_assert!(!op.components[c].is_fixed);

        // Add tiny random perturbation.
        let perturbation: db::Vector<f64> = {
            let r = op.components[c].size;
            let w = r.width();
            let h = r.height();
            // Move the cell at most by a fraction of its width and height.
            let dx = w * 0.05;
            let dy = h * 0.05;

            db::Vector::new(rng.gen_range(-dx..dx), rng.gen_range(-dy..dy))
        };

        *pos += perturbation;
    }
}

/// Run the optimization loop.
fn run_optimization<C>(
    max_iter: usize,
    initial_density_penalty: f64,
    mut op: OptimizationProblem<C>,
) -> OptimizationProblem<C>
where
    C: db::NetlistIds,
    C::CellInstId: Sync + Send,
    C::NetId: Sync,
{
    assert!(initial_density_penalty >= 0.);
    assert!(initial_density_penalty <= 1.);

    // Get initial values.
    let param: Vec<_> = op
        .movable_components
        .iter()
        .map(|&idx| op.components[idx].position.displacement)
        .collect();
    debug_assert!(param.iter().all(|v| v.x.is_finite() && v.y.is_finite()));
    let mut param = ParVec::from(param);

    // Weight of density cost.
    // This is low in the beginning and is steadily increased
    // towards the end of the optimization.
    let max_density_penalty = 0.95;
    let mut density_penalty = initial_density_penalty.min(max_density_penalty);

    // Buffer for previous parameters and gradient.
    let mut prev_parameter_and_gradient: Option<(ParVec<_>, ParVec<_>)> = None; // v[k-1] and grad(f(v[k-1]))

    // Timer to limit log output.
    let mut last_log_output_time = std::time::Instant::now();
    let log_output_interval = std::time::Duration::from_secs(10); // Time between log outputs.

    let seed = 7; // Seed for random numbers.
    perturb_positions(&op, &mut param, seed);
    op.limit_positions_to_bounding_box(&mut param);

    for i in 0..max_iter {
        // Decide if INFO log output should be printed in this iteration.
        let do_print_info = {
            let time_elapsed = last_log_output_time.elapsed() >= log_output_interval;
            i % 100 == 0 || time_elapsed || i == 0 || i + 1 == max_iter
        };

        if do_print_info {
            // Reset timer.
            last_log_output_time = std::time::Instant::now();
        }

        if do_print_info {
            log::info!("Iteration {}/{}", i + 1, max_iter);
        } else {
            log::debug!("Iteration {}/{}", i + 1, max_iter);
        }

        // Steadily increase the density penalty to converge to the target density.
        density_penalty = max_density_penalty - (max_density_penalty - density_penalty) * 0.995;

        log::debug!("density penalty = {:0.4}", density_penalty);

        let gradient = match op.compute_gradient(do_print_info, &param, density_penalty) {
            Some(value) => value,
            None => break,
        };

        // 3 ) Estimate Lipschitz constant.
        let lipschitz_constant =
            if let Some((prev_param, prev_gradient)) = prev_parameter_and_gradient {
                // L = norm2(grad(f(v[k])) - grad(f(v[k-1]))) / norm2(v[k] - v[k-1])

                let denom_sq: f64 = gradient
                    .inner
                    .par_iter()
                    .zip(&prev_gradient.inner)
                    .map(|(&g, &g_prev)| (g - g_prev).norm2_squared())
                    .kahan_sum()
                    .max(1e-12);
                let nom_sq: f64 = param
                    .par_iter()
                    .zip(&prev_param.inner)
                    .map(|(&p, &p_prev)| (p - p_prev).norm2_squared())
                    .kahan_sum()
                    .max(1e-12); // Avoid division by zero.

                //assert_ne!(
                //    nom_sq, 0.,
                //    "Optimization parameter must differ between two iterations."
                //);

                denom_sq.sqrt() / nom_sq.sqrt()
            } else {
                // Cannot yet tell since no iteration was done yet. Start pessimistic.
                1e9 // TODO compute from second order gradient.
            };

        log::debug!("Lipschitz constant L = {}", lipschitz_constant);

        let step_size = 0.8 / lipschitz_constant;

        // Store a copy of the parameter vector before modifying it.
        let prev_param = param.clone();

        // Update optimization parameter.
        param
            .inner
            .par_iter_mut()
            .zip(&gradient.inner)
            .for_each(|(p, grad)| *p -= *grad * step_size);
        debug_assert!(param.iter().all(|v| v.x.is_normal() && v.y.is_normal()));

        //let seed = 42 + i as u64; // Seed for random numbers.
        //self.perturb_positions(&op, &mut param, seed);
        op.limit_positions_to_bounding_box(&mut param);

        prev_parameter_and_gradient = Some((prev_param, gradient));

        // Update component locations.
        op.movable_components
            .iter() // TODO: Parallelize
            .zip(&param.inner)
            .for_each(|(&c, &pos)| {
                debug_assert!(!op.components[c].is_fixed);
                op.components[c].position.displacement = pos;
            });
    }

    op
}

impl<C> MixedSizePlacer<C> for MixedSizeGlobalPlacer
where
    C: db::L2NBase<Coord = db::Coord>,
    C::CellInstId: Sync + Send,
    C::NetId: Sync,
{
    fn name(&self) -> &str {
        "ePlace_mGP"
    }

    fn find_cell_positions_impl(
        &self,
        placement_problem: &dyn PlacementProblem<C>,
    ) -> Result<HashMap<C::CellInstId, db::SimpleTransform<db::Coord>>, PlacementError> {
        let chip = placement_problem.fused_layout_netlist();
        let top_cell = placement_problem.top_cell();

        let top_ref = chip.cell_ref(&top_cell);

        // Find number of cells to be placed.
        let num_cells = top_ref.num_child_instances();

        if num_cells == 0 {
            // Trivial solution.
            // Return such that the rest of the program can assume there's at least one cell.
            return Ok(HashMap::new());
        }

        let placement_instance = PlacementInstance::new(self, placement_problem)?;

        log::info!("Target density: {:.4}", self.target_density);

        placement_instance.report_cells_without_outline();

        let op = placement_instance.create_optimization_problem();
        let op = run_optimization(self.max_iter, self.initial_density_penalty, op);

        // Create result hash-map.
        let result: HashMap<_, _> = op
            .components
            .iter()
            .filter(|c| !c.is_fixed) // Don't output locations of fixed instances.
            .filter_map(|comp| {
                let pos = comp.position.cast(); // Convert to data base coordinates.
                comp.id.as_ref().map(|id| (id.clone(), pos)) // Skip the location of the top cell (has id = None).
            })
            .collect();

        Ok(result)
    }
}

/// A value like the total wirelength or electrostatic energy
/// together with the gradient and second order gradient.
///
/// The gradient represents the "force" for each cell.
#[derive(Debug, Clone)]
struct ValueWithGradients {
    value: f64,
    /// Gradient.
    gradient: ParVec<db::Vector<f64>>,
    /// Diagonal of Hessian matrix.
    second_order_gradient: ParVec<db::Vector<f64>>,
}

/// Compute the weighted forces (wire length gradient) on the components caused by all the nets
/// and the total wire-length.
/// Both forces and wire-length are weighted with the weights of the nets.
/// Compute the gradient of `positions`.
///
/// * `nets`: Iterator all nets. Produces `(net_weight, terminal_location)` tuples.
/// * `I`: Iterator over `(net weight, net components)` tuples.
/// * `positions`: Optimization parameter:
/// * `components`: List of components. Needed to find the pin positions.
/// * `core_box`: Bounding box of placement region. Used to determine the value of `gamma`.
///
/// Returns the total wire length.
fn compute_wirelength_and_gradient<
    'a,
    I: ParallelIterator<Item = (f64, &'a [ComponentPin])>,
    ComponentId: Sync,
>(
    nets: I,
    components: &[Component<ComponentId>],
    positions: &[db::Vector<f64>],
    core_box: db::Rect<f64>,
) -> ValueWithGradients {
    assert_eq!(components.len(), positions.len());
    debug_assert!(
        positions.iter().all(|p| p.x.is_finite() && p.y.is_finite()),
        "Positions cannot be inf or NaN."
    );

    // Choose gamma such that f64 overflows don't happen.
    // Make sure that: sum(exp(xi / gamma)) < f64::MAX
    let gamma = {
        let magnitude_x = core_box
            .lower_left()
            .x
            .abs()
            .max(core_box.upper_right().x.abs());
        let magnitude_y = core_box
            .lower_left()
            .y
            .abs()
            .max(core_box.upper_right().y.abs());

        let magnitude = magnitude_x.max(magnitude_y);

        // TODO: This is a simple guess that no net has more pins than total amount of components.
        let max_net_cardinality = components.len();
        let margin = 2.;
        margin * magnitude / f64::ln(f64::MAX / max_net_cardinality as f64)
    };

    let identity = || {
        (
            ParVec::zeros(components.len()),
            ParVec::zeros(components.len()),
            KahanAccumulator::new(),
        )
    };

    let (gradient_buffer, second_order_gradient_buffer, wirelength_sum) = nets
        .fold(identity, |accumulator, net| {
            let (mut gradient_buf, mut second_order_gradient_buf, mut wirelength_sum) = accumulator;
            let (weight, components_of_net) = net;

            // Find pin locations.
            let points = components_of_net.iter().map(|pin| {
                let pos = positions[pin.component_idx.0]; // Get current location of the cell.
                let tf = db::SimpleTransform {
                    // Keep rotation/mirroring but use current location.
                    displacement: pos,
                    ..components[pin.component_idx.0].position
                };
                // Compute the actual position of the center of the cell.
                tf.transform_point(pin.relative_location)
            });

            let (wl_x, wl_y) = log_sum_exp_wire_length(points.clone(), gamma);
            wirelength_sum.update((wl_x + wl_y) * weight);

            // Compute forces of this net.
            let gradient = log_sum_exp_wire_length_gradient(points.clone(), gamma);

            // Accumulate forces.
            for (pin, force) in components_of_net.iter().zip(gradient) {
                gradient_buf[pin.component_idx.0] += force * weight;
            }

            // Compute second order gradient of wire-length approximation. Accumulate values into scratch vector.
            let gradient2 = log_sum_exp_wire_length_second_order_gradient(points, gamma);

            // Accumulate.
            for (pin, force) in components_of_net.iter().zip(gradient2) {
                second_order_gradient_buf[pin.component_idx.0] += force * weight;
            }

            (gradient_buf, second_order_gradient_buf, wirelength_sum)
        })
        .reduce(identity, |a, b| {
            let (g_a, g2_a, wl_a) = a;
            let (g_b, g2_b, wl_b) = b;

            (&g_a + &g_b, &g2_a + &g2_b, wl_a + wl_b)
        });

    ValueWithGradients {
        gradient: gradient_buffer,
        second_order_gradient: second_order_gradient_buffer,
        value: wirelength_sum.sum(),
    }
}

/// 'Filler' insertion.
///
/// Insert positively charged fillers such that the total charge gets zero.
///
/// Instead of putting filler cells as ePlace-MS does,
/// we directly update the negative-density bins like there
/// would be nicely distributed fillers. Bins with positive charge density
/// don't get changed.
///
/// * `density_bins`: Bins to be updated.
fn insert_filler_density(density_bins: &mut DensityMap<f64, Complex<f64>>) {
    let charge_sum: f64 = density_bins
        .get_data_ref()
        .par_iter()
        .map(|v| v.re)
        .kahan_sum();

    let tolerance = 1e-2; // Tolerance for comparing floating point values.

    if charge_sum > tolerance {
        // TODO: is this correct?
        log::debug!("charge_sum = {}", charge_sum);
        log::error!("Target density is too low and cannot be reached.");
    }

    assert!(
        charge_sum <= 0. + tolerance,
        "Cannot compensate positive charge by inserting positively charged fillers."
    );

    let charge_underflow = -charge_sum.min(0.); // That amount needs to be added.

    // Compute the sum of all negative bins.
    let negative_charge_sum = -density_bins
        .get_data_ref()
        .par_iter()
        .filter(|&&v| v.re < 0.)
        .map(|v| v.re)
        .kahan_sum();

    // Add positive charges to negative bins.
    // Total amount of added charge = filler_area.
    density_bins.get_data_ref_mut().iter_mut().for_each(|v| {
        if v.re < 0.0 {
            let abs = -v.re;
            debug_assert!(abs >= -tolerance);
            // Fill with positive charge proportional to the current negativeness.
            v.re += charge_underflow * abs / negative_charge_sum;
        }
    });

    // {
    //     // Distribute missing charge equally on all bins.
    //     let num_bins = density_bins.get_data_ref().len();
    //
    //     let charge_increment = -charge_sum / (num_bins as f64);
    //     density_bins.get_data_ref_mut()
    //         .iter_mut()
    //         .for_each(|v| {
    //             v.re += charge_increment;
    //         });
    // }

    // Check that filler insertion properly balanced the charges.

    let total_charge = density_bins.get_data_ref().iter().map(|v| v.re).kahan_sum();

    debug_assert!(
        total_charge < 1e-3,
        "Total charge must be zero but is {}.",
        total_charge
    );
}

/// Representation of a cell (or filler) to be placed.
#[derive(Clone, PartialEq, Debug)]
struct Component<ComponentId> {
    /// Id of the cell instance.
    /// Only the top cell and filler cells have the `None` value.
    id: Option<ComponentId>,
    /// Bounding box of the cell template.
    size: db::Rect<f64>,
    /// The charge density of this component. This should b `1.0` for standard cells
    /// and `target_density` for larger blocks.
    charge_density: f64,
    /// Current position and orientation of the cell.
    position: db::SimpleTransform<f64>,
    /// Is this a non-movable cell?
    is_fixed: bool,
    /// This is a filler cell.
    is_filler: bool,
}

impl<C> Component<C> {
    /// Bounding box of placed component.
    fn actual_shape(&self) -> db::Rect<f64> {
        self.size.transform(|p| self.position.transform_point(p))
    }
}

/// Draw all cells onto the canvas with density = 1.
fn draw_components<C>(density: &mut EDensity<f64>, cells: &[Component<C>]) {
    let boundary = density.charge_distribution.dimension;
    for cell in cells {
        // Move to actual place.
        if cell.is_filler || cell.id.is_some() {
            // Don't draw the top cell. Compensate for bug - zero-sized rectangle of the top-cell is drawn wrong.
            let shape = cell.actual_shape();
            debug_assert!(boundary.contains_rectangle(&shape));

            //let area = shape.width() * shape.height();
            //let charge = area * cell.charge_density;
            //density.draw_point_charge(shape.center(), charge);
            density.draw_charge_density(&shape, cell.charge_density);
        }
    }
}

/// Get simplified pin locations for all cells.
/// For each pin, an average location of its shapes is chosen.
/// They are used do make the global placement aware of the pin locations of macro cells.
fn extract_pin_locations<C: L2NBase>(
    chip: &C,
) -> HashMap<C::CellId, HashMap<C::PinId, db::Point<C::Coord>>>
where
    C::Coord: FromPrimitive,
{
    let mut pin_locations = HashMap::new();

    for cell in chip.each_cell() {
        let mut cell_pin_locations = HashMap::new();

        for pin in chip.each_pin(&cell) {
            // Get centers of all pin shapes.
            let pin_locations: Vec<_> = chip
                .shapes_of_pin(&pin)
                .filter_map(|shape_id| {
                    chip.with_shape(&shape_id, |_layer, shape| {
                        shape.try_bounding_box().map(|bbox| bbox.center())
                    })
                })
                .collect();
            // Compute the average pin location as an approximation if there are multiple pins.
            if !pin_locations.is_empty() {
                let sum: db::Point<C::Coord> = pin_locations.iter().copied().sum();
                let avg = sum / C::Coord::from_usize(pin_locations.len()).unwrap();

                cell_pin_locations.insert(pin, avg);
            }
        }

        pin_locations.insert(cell, cell_pin_locations);
    }

    pin_locations
}

// Prepare data for FFT.
struct OptimizationProblem<'a, C: NetlistIds> {
    components: Vec<Component<C::CellInstId>>,
    num_filler_cells: usize,
    /// Core boundary which components cannot leave.
    boundary: db::Rect<f64>,
    /// Enlarged boundary.
    fft_boundary: db::Rect<f64>,
    /// Regions where cells can be placed.
    /// A list of non-overlapping rectangles.
    placement_sites: Vec<db::Rect<db::SInt>>,
    /// Placement density.
    target_density: f64,
    /// Indices of movable (not fixed) components.
    movable_components: Vec<usize>,
    fft_kernel: EFieldKernel<f64>,
    /// Electric charge density which represents the placement regions and obstacles
    /// but does not include the cells.
    default_density: EDensity<f64>,
    net_weights: &'a FnvHashMap<C::NetId, f64>,
    /// Lookup table for components attached to a net, including relative pin locations.
    /// Used for computing wirelength.
    component_indices_by_net: AdjacencyList<C::NetId>,
}

impl<'a, C: NetlistIds> Gradient for OptimizationProblem<'a, C>
where
    C::NetId: Sync,
    C::CellInstId: Sync,
{
    type Param = ParVec<db::Vector<f64>>;

    type Gradient = ParVec<db::Vector<f64>>;

    fn gradient(&self, param: &Self::Param) -> Result<Self::Gradient, argmin::core::Error> {
        let density_penalty = 0.7; // TODO
        let gradient = self.compute_gradient(false, &param.inner, density_penalty);

        match gradient {
            Some(g) => Ok(g.into()),
            None => Err(todo!()),
        }
    }
}

/// Index of a component in the list of all components.
#[derive(Copy, Clone, PartialEq, PartialOrd, Ord, Eq, Hash, Debug)]
struct ComponentIdx(usize);

#[derive(Copy, Clone, Debug, PartialEq)]
struct ComponentPin {
    /// Index of the component which this pin belongs to.
    component_idx: ComponentIdx,
    /// Location of the pin relative to the component.
    relative_location: db::Point<f64>,
}

/// Space-efficient representation of nets and their connected component pins.
struct AdjacencyList<NetId> {
    /// Unique Net IDs together with a range which points to a slice of the `pins` list.
    nets: Vec<(NetId, std::ops::Range<usize>)>,
    /// Tuples of `(component index, relative pin location)`.
    pins: Vec<ComponentPin>,
}

impl<NetId> AdjacencyList<NetId> {
    fn new() -> Self {
        Self {
            nets: vec![],
            pins: vec![],
        }
    }

    /// Get the number of the nets in this list.
    fn num_nets(&self) -> usize {
        self.nets.len()
    }

    /// Append a net and its adjacent pins.
    /// The net must not yet exist in the list.
    fn add_net(&mut self, net_id: NetId, pins: impl Iterator<Item = ComponentPin>)
    where
        NetId: Eq,
    {
        debug_assert!(
            self.nets.iter().all(|(n, _)| n != &net_id),
            "net is already in the list"
        );

        let start_idx = self.pins.len();
        self.pins.extend(pins);
        let end_idx = self.pins.len();

        self.nets.push((net_id, start_idx..end_idx));
    }

    /// Iterate through all nets and their adjacent pins.
    fn iter_nets(&self) -> impl Iterator<Item = (&NetId, &[ComponentPin])> {
        self.nets
            .iter()
            .map(|(net_id, pins_range)| (net_id, &self.pins[pins_range.start..pins_range.end]))
    }

    /// Iterate through all nets and their adjacent pins.
    fn par_iter_nets(&self) -> impl ParallelIterator<Item = (&NetId, &[ComponentPin])>
    where
        NetId: Sync,
    {
        self.nets
            .par_iter()
            .map(|(net_id, pins_range)| (net_id, &self.pins[pins_range.start..pins_range.end]))
    }
}

impl<'a, C> OptimizationProblem<'a, C>
where
    C: NetlistIds,
    C::CellInstId: Sync,
    C::NetId: Sync,
{
    // Initialize the rasterized charge density.
    fn init_charge_density(&self) -> EDensity<f64> {
        let mut density = self.default_density.clone();

        // Update the densities with the cells including (filler cells).
        draw_components(&mut density, &self.components);

        // 'Filler' insertion.
        // Make the total charge zero by inserting positively charged fillers.
        insert_filler_density(&mut density.charge_distribution);

        let charge_sum: f64 = density
            .charge_distribution
            .get_data_ref()
            .iter()
            .map(|v| v.re)
            .kahan_sum();
        debug_assert!(charge_sum.abs() < 1e-3, "netto charge should be zero");

        density
    }

    fn compute_gradient(
        &self,
        do_print_info: bool,
        param: &[db::Vector<f64>],
        density_penalty: f64,
    ) -> Option<ParVec<db::Vector<f64>>>
    where
        C::CellInstId: Sync,
        C::NetId: Sync,
    {
        assert_eq!(param.len(), self.movable_components.len());

        assert!(
            (0. ..=1.).contains(&density_penalty),
            "density penalty out of range"
        );

        let density = self.init_charge_density();

        if self.is_optimization_loop_target_reached(&density, do_print_info) {
            return None;
        }

        // 1) Compute current gradient.
        let e_energy = self.compute_electric_forces(&density, param);
        let wire_length = self.compute_wire_forces(param);

        debug_assert_eq!(e_energy.gradient.len(), wire_length.gradient.len());
        debug_assert_eq!(
            e_energy.second_order_gradient.len(),
            wire_length.second_order_gradient.len()
        );
        if do_print_info {
            log::info!("Wire length: {:.3} dbu", wire_length.value);
        } else {
            log::debug!("Wire length: {:.3} dbu", wire_length.value);
        }
        log::debug!("Electrostatic energy: {}", e_energy.value);

        let (weight_wl, weight_d) = {
            // Balance wirelength and density.

            // Compute 2-norms of gradients.
            let density_gradient_norm2: f64 = e_energy.gradient.l2_norm();
            let wire_length_gradient_norm2: f64 = wire_length.gradient.l2_norm();
            let gradient_ratio = (wire_length_gradient_norm2 + 1.) / (density_gradient_norm2 + 1.);
            log::trace!(
                "gradient ratio (wirelength gradient / density gradient) = {:0.8}",
                gradient_ratio
            );

            // let weight_d = density_penalty + (1. - density_penalty) * gradient_ratio;
            let weight_d = density_penalty * gradient_ratio;

            // Compute weights for wirelength and density.
            // let weight_d = density_penalty;
            let weight_wl = 1. - density_penalty;

            if do_print_info {
                log::debug!("weight_wl = {:0.8}, weight_d = {:0.8}", weight_wl, weight_d);
                log::info!("density penalty = {:0.4}", density_penalty);
            }

            (weight_wl, weight_d)
        };

        // Use weighted average of density penalty and wire length.
        let _cost = weight_wl * wire_length.value + weight_d * e_energy.value;

        let gradient = wire_length
            .gradient
            .zip_map(&e_energy.gradient, |&wl, &e| wl * weight_wl + e * weight_d);

        // Compute the second order gradient with the same weights.
        // The second order gradient will be used for the hessian pre-conditioning.
        // TODO: Compute second order gradient from parameters of previous step ( would be more efficient).
        let second_order_gradient = wire_length
            .second_order_gradient
            .zip_map(&e_energy.second_order_gradient, |&wl, &e| {
                wl * weight_wl - e * weight_d
            });

        // 2) Hessian preconditioning.
        let use_preconditioning = false;
        let gradient = if use_preconditioning {
            let min_preconditioning = 0.01; // Make sure not to divide with very small numbers.
            let f = second_order_gradient.inner.par_iter().map(|g| {
                db::Vector::new(
                    g.x.abs().max(min_preconditioning),
                    g.y.abs().max(min_preconditioning),
                )
            });
            // Precondition the gradient vector.
            let mut gradient = gradient;

            gradient.inner.par_iter_mut().zip(f).for_each(|(g, f)| {
                g.x /= f.x;
                g.y /= f.y;
            });
            gradient
        } else {
            gradient
        };
        Some(gradient)
    }

    /// Compute wire length and gradient.
    /// `positions`: Optimization parameter.
    fn compute_wire_forces(&self, positions: &[db::Vector<f64>]) -> ValueWithGradients
    where
        C::NetId: Sync,
        C::CellInstId: Sync,
    {
        // Expand parameter vector to also hold the positions of non-movable components.
        let mut all_positions: Vec<_> = self
            .components
            .iter()
            .map(|c| c.position.displacement)
            .collect();
        self.movable_components
            .iter()
            .zip(positions)
            .for_each(|(&idx, &pos)| all_positions[idx] = pos);

        // Sanity check on array sizes.
        assert_eq!(positions.len(), self.movable_components.len());
        assert_eq!(all_positions.len(), self.components.len());

        // Compute wirelength and gradient.
        let ValueWithGradients {
            gradient: mut net_wl_gradient,
            second_order_gradient: mut net_wl_second_order_gradient,
            value: wirelength,
        } = {
            // Get nets and their weights.
            let nets = self
                .component_indices_by_net
                .par_iter_nets()
                .map(|(net, components)| {
                    (
                        self.net_weights.get(net).copied().unwrap_or(1.0),
                        components,
                    )
                });

            compute_wirelength_and_gradient(nets, &self.components, &all_positions, self.boundary)
        };

        if wirelength.is_nan() {
            log::error!("Wire length is NaN.");
        }

        // Use only forces of movable components.
        {
            // Check that the indices are sorted.
            debug_assert!(
                self.movable_components
                    .iter()
                    .zip(self.movable_components.iter().skip(1))
                    .all(|(a, b)| a < b),
                "movable_components must be sorted."
            );

            for (write_idx, &read_idx) in self.movable_components.iter().enumerate() {
                net_wl_gradient[write_idx] = net_wl_gradient[read_idx];
                net_wl_second_order_gradient[write_idx] = net_wl_second_order_gradient[read_idx];
            }

            net_wl_gradient.truncate(self.movable_components.len());
            net_wl_second_order_gradient.truncate(self.movable_components.len());
        }

        ValueWithGradients {
            gradient: net_wl_gradient,
            second_order_gradient: net_wl_second_order_gradient,
            value: wirelength,
        }
    }

    /// Compute the density and it's gradient and diagonal of the hessian matrix..
    /// `positions`: Optimization parameter.
    fn compute_electric_forces(
        &self,
        density: &EDensity<f64>,
        positions: &[db::Vector<f64>],
    ) -> ValueWithGradients
    where
        C::CellInstId: Sync,
    {
        // Sanity check on array sizes.
        assert_eq!(positions.len(), self.movable_components.len());

        let efield = density.compute_efield(&self.fft_kernel);

        //dump_array2(
        //    "/tmp/density.csv",
        //    &self
        //        .density
        //        .charge_distribution
        //        .get_data_ref()
        //        .mapv(|c| c.re),
        //);

        // Compute the total energy in the potential field.
        // This is a measure for the density mismatch. The goal
        // of the optimization is to bring the energy down to zero.
        let energy: f64 = density
            .charge_distribution
            .data
            .par_iter()
            .map(|p| p.re * p.re)
            .kahan_sum();

        // Compute forces on all movable components.
        let (gradient, second_order_gradient): (Vec<_>, Vec<_>) = self
            .movable_components
            .par_iter()
            .zip_eq(positions.par_iter())
            .map(|(&component_idx, &displacement)| {
                let component = &self.components[component_idx];

                let position = db::SimpleTransform {
                    displacement,
                    ..component.position
                };

                // Compute positioned shape of the cell.
                let r = component.size.transform(|p| position.transform_point(p));

                // Compute force and force derivatives (f_x/dx and f_y/dy).
                //let area = r.width() * r.height();
                //let charge = area * c.charge_density;
                //let (force, force_gradient) =
                //    { efield.compute_force_on_point_charge(r.center(), charge) };
                let (force, force_gradient) = efield.compute_force(&r, component.charge_density);

                (-force, force_gradient)
            })
            .unzip();

        ValueWithGradients {
            value: energy,
            gradient: gradient.into(),
            second_order_gradient: second_order_gradient.into(),
        }
    }

    /// Limit component positions to core bounding box.
    fn limit_positions_to_bounding_box(&self, positions: &mut [db::Vector<f64>]) {
        self.movable_components
            .par_iter()
            .zip(positions.par_iter_mut())
            .for_each(|(&c, pos)| {
                debug_assert!(!self.components[c].is_fixed);

                // Limit position to core area box.
                let r = {
                    // Find the actual bounding-box of the component.
                    let component = &self.components[c];
                    let mut transform = component.position;
                    transform.displacement = *pos;
                    component.size.transform(|p| transform.transform_point(p))
                };
                // Find shift amount to bring the rectangle into the boundary box.
                let mut correction = db::Vector::zero();

                correction.x += (self.boundary.lower_left.x - r.lower_left.x).max(0.);
                correction.x += (self.boundary.upper_right.x - r.upper_right.x).min(0.);
                correction.y += (self.boundary.lower_left.y - r.lower_left.y).max(0.);
                correction.y += (self.boundary.upper_right.y - r.upper_right.y).min(0.);

                *pos += correction;
                let limited_pos = *pos + correction;
                *pos = limited_pos;
            });
    }

    fn is_optimization_loop_target_reached(
        &self,
        density: &EDensity<f64>,
        do_print_info: bool,
    ) -> bool {
        // Check stopping condition. Stop when density overflow is close to zero.

        // Downsample the charge resolution: Compute average over larger window.
        // let downsampling_factor = 2*((1. / self.target_density).sqrt()) as usize;
        let original_resolution = density.charge_distribution.get_data_ref().dim();
        let downsampling_factor = 8;
        let resolution = (
            original_resolution.0 / downsampling_factor,
            original_resolution.1 / downsampling_factor,
        );

        let charge_distribution = {
            // Draw density map (without filler cells).
            let mut density: EDensity<f64> = EDensity {
                charge_distribution: DensityMap::new(self.fft_boundary, resolution),
            };
            density.set(self.target_density);
            // Set density at placement sites to zero.
            for r in &self.placement_sites {
                density.draw_charge_density(&r.cast(), -self.target_density);
            }

            let num_normal_cells = self.components.len() - self.num_filler_cells;

            // Sanity check:
            assert!(
                self.components
                    .get(num_normal_cells - 1)
                    .map(|c| !c.is_filler)
                    .unwrap_or(true),
                "num_filler_cells is wrong or filler cells are not strictly appended to the list"
            );
            // Sanity check:
            assert!(
                self.components
                    .get(num_normal_cells)
                    .map(|c| c.is_filler)
                    .unwrap_or(true),
                "num_filler_cells is wrong or filler cells are not strictly appended to the list"
            );
            // Extensive sanity check:
            debug_assert!(
                self.components
                    .iter()
                    .enumerate()
                    .all(|(idx, c)| c.is_filler == (idx >= num_normal_cells)),
                "filler cells must be appended at the end of the list of components"
            );

            draw_components(&mut density, &self.components[0..num_normal_cells]);

            density.charge_distribution
        };

        //let charge_distribution = self
        //    .density
        //    .charge_distribution
        //    .downsample(downsampling_factor);

        let bin_area = charge_distribution.bin_area();

        let overflow_threshold = 0.0; // Net charge in a bin is considered an overflow if larger than this value.

        // Compute density overflow.
        let density_overflow = charge_distribution
            .get_data_ref()
            .par_iter()
            .map(|v| v.re / bin_area)
            .filter(|&v| v > overflow_threshold)
            .kahan_sum();

        let density_overflow_max: f64 = charge_distribution
            .get_data_ref()
            .par_iter()
            .map(|v| v.re / bin_area)
            .filter(|&v| v > overflow_threshold)
            .map(|v| v - overflow_threshold)
            .fold_with(0.0, |max, v| if v > max { v } else { max }) // Find the maximum value.
            .reduce(|| 0.0, |a, b| if b > a { b } else { a }); // Find the maximum value.

        let num_overflows = charge_distribution
            .get_data_ref()
            .par_iter()
            .map(|v| v.re / bin_area)
            .filter(|&v| v > overflow_threshold)
            .count();

        let density_overflow_avg = if num_overflows == 0 {
            0.
        } else {
            density_overflow / num_overflows as f64
        };

        let density_overflow_margin = 0.05;
        if do_print_info || density_overflow_max < density_overflow_margin {
            log::info!(
                "Density overflow: avg={:.4}, max={:.4}",
                density_overflow_avg,
                density_overflow_max
            );
        } else {
            log::debug!(
                "Density overflow: avg={:.4}, max={:.4}",
                density_overflow_avg,
                density_overflow_max
            );
        }

        if density_overflow_max < density_overflow_margin {
            log::info!(
                "Target density reached (within {:.0}%).",
                density_overflow_margin * 100.
            );
            return true;
        }

        false
    }
}

#[test]
fn test_density_optimization() {
    // Test spreading a small number of cells which are originally at the same location.

    let target_density = 0.5; // Not really used here.
    let core_bbox = db::Rect::new((-50, -50), (50, 50));
    let fft_boundary = core_bbox.scale(2);
    assert_eq!(core_bbox.center(), fft_boundary.center());

    let new_component = |id: usize| Component {
        id: Some(id),
        size: db::Rect::new((-1., -1.), (1., 1.)),
        charge_density: 1.,
        position: db::SimpleTransform::translate(db::Vector::new(0., 0.)),
        is_fixed: false,
        is_filler: false,
    };

    let num_components = 100;

    let components: Vec<_> = (0..num_components).map(|i| new_component(i)).collect();

    let net_weights = Default::default(); // There's no nets.

    let movable_components = components
        .iter()
        .enumerate()
        .filter_map(|(idx, c)| (!c.is_fixed).then_some(idx))
        .collect();

    let resolution = (200, 200);
    let kernel = EFieldKernel::new(fft_boundary.cast(), resolution);
    let mut default_density = kernel.new_density_map();

    // Mark core area with negative charge density.
    default_density.draw_charge_density(&core_bbox.cast(), -1.);

    struct MockIds;

    impl HierarchyIds for MockIds {
        type CellId = ();
        type CellInstId = usize;
    }
    impl NetlistIds for MockIds {
        type PinId = ();
        type PinInstId = ();
        type NetId = ();
    }

    let op: OptimizationProblem<MockIds> = OptimizationProblem {
        components,
        boundary: core_bbox.cast(),
        fft_boundary: fft_boundary.cast(),
        fft_kernel: kernel,
        movable_components,
        default_density,
        net_weights: &net_weights,
        component_indices_by_net: AdjacencyList::new(), // There's no nets yet.
        target_density,
        placement_sites: vec![core_bbox],
        num_filler_cells: 0,
    };

    let op = run_optimization(100, 1.0, op);

    dbg!(&op.components);

    let density = op.init_charge_density();
    assert!(op.is_optimization_loop_target_reached(&density, false));
}
